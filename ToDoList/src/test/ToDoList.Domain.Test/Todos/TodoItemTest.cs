﻿using Shouldly;
using System;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;
using Xunit;

namespace ToDoList.Domain.Test.Todos
{
    public class TodoItemTest
    {
        [Fact]
        public void ShouldHaveCorrectInitialData()
        {
            TodoItem todo = new TodoItem();
            todo.Id.ShouldBe(Guid.Empty);
            todo.UserId.ShouldBe(Guid.Empty);
            todo.ListId.ShouldBe(Guid.Empty);
            todo.Title.ShouldBeNull();
            todo.Note.ShouldBeNull();
            todo.DueDate.ShouldBeNull();
            todo.ReminderDate.ShouldBeNull();            
            todo.CreationTime.ShouldBe(DateTimeOffset.MinValue);
            todo.IsDeleted.ShouldBe(false);
            todo.User.ShouldBeNull();
            todo.List.ShouldBeNull();
            todo.Reminders.ShouldBeNull();
        }

        [Fact]
        public void ShouldHaveCorrectProperties()
        {
            Guid id = Guid.NewGuid();
            DateTimeOffset dtNow = DateTimeOffset.Now;

            TodoItem todo = new TodoItem
            {
                Id = id,
                UserId = id,
                ListId = id,
                Title = "Test",
                Note = "Note",
                DueDate = dtNow,
                ReminderDate = dtNow,
                Status = TodoStatus.Completed,
                CreationTime = dtNow,
                IsDeleted = true
            };

            todo.Id.ShouldBe(id);
            todo.UserId.ShouldBe(id);
            todo.ListId.ShouldBe(id);
            todo.Title.ShouldBe("Test");
            todo.Note.ShouldBe("Note");
            todo.DueDate.ShouldBe(dtNow);
            todo.ReminderDate.ShouldBe(dtNow);
            todo.Status.ShouldBe(TodoStatus.Completed);
            todo.CreationTime.ShouldBe(dtNow);
            todo.IsDeleted.ShouldBe(true);
            todo.User.ShouldBeNull();
            todo.List.ShouldBeNull();
            todo.Reminders.ShouldBeNull();

        }
    }
}
