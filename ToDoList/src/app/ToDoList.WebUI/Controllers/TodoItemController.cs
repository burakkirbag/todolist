﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace ToDoList.WebUI.Controllers
{
    public class TodoItemController : Controller
    {
        public IActionResult Index([FromQuery]Guid listId)
        {
            ViewBag.TodoListId = listId;
            return View();
        }

        public IActionResult Add([FromQuery]Guid listId)
        {
            ViewBag.TodoListId = listId;
            return View();
        }

        public IActionResult Edit([FromQuery]Guid listId, [FromQuery]Guid id)
        {
            ViewBag.TodoListId = listId;
            ViewBag.TodoItemId = id;
            return View();
        }
    }
}
