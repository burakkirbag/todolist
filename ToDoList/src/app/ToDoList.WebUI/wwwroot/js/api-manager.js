﻿function checkResponse(response) {
    if (response.status === 200) {
        if (response.data) {
            if (response.data.success) {
                return response.data;
            }
            else {
                throw response.data;
            }
        }
    }

    return response;
}

function checkError(error) {
    if (error) {
        if (error.response && error.response.status === 401) {
            userInfoKill();
            alert("Oturumunuz sona erdi. Lütfen tekrar giriş yapın.");
            window.location = "/";
        }

        if (error.code && error.code === 401) {
            userInfoKill();
            alert(error.message);
            window.location = "/";
        } else if (error.code && (error.code === 400 || error.code === 500)) {

            let alertMsg = error.message;
            if (error.validations && error.validations.length > 0) {
                error.validations.forEach((validation) => {
                    alertMsg += "\n" + validation.field + " : " + validation.message;
                });
            }
            alert(alertMsg);
        }

        return error;
    }
}

function setAxiosConfig() {

    let token = localStorage.getItem('user_token');
    axios.defaults.baseURL = 'http://localhost:53240';
    axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    axios.defaults.headers.common['Content-Type'] = 'application/json';
    axios.defaults.withCredentials = false;
    axios.defaults.credentials = 'same-origin';
}

function getApi(url, onSuccess, onError) {

    setAxiosConfig();

    axios.get(url)
        .then(obj => {
            if (onSuccess) {
                onSuccess(checkResponse(obj));
            }
        }).catch((err) => {
            if (onError) {
                onError(checkError(err));
            }
        });
}

function getApiWithParams(url, params, onSuccess, onError) {

    setAxiosConfig();

    axios.get(url, { params: params })
        .then(obj => {
            if (onSuccess) {
                onSuccess(checkResponse(obj));
            }
        }).catch((err) => {
            if (onError) {
                onError(checkError(err));
            }
        });
}

function postApi(url, data, onSuccess, onError) {

    setAxiosConfig();

    axios.post(url, data)
        .then(obj => {
            if (onSuccess) {
                onSuccess(checkResponse(obj));
            }
        }).catch((err) => {
            if (onError) {
                onError(checkError(err));
            }
        });
}

function deleteApi(url, onSuccess, onError) {

    setAxiosConfig();

    axios.delete(url)
        .then(obj => {
            if (onSuccess) {
                onSuccess(checkResponse(obj));
            }
        }).catch((err) => {
            if (onError) {
                onError(checkError(err));
            }
        });
}

function putApi(url, data, onSuccess, onError) {

    setAxiosConfig();

    axios.put(url, data)
        .then(obj => {
            if (onSuccess) {
                onSuccess(checkResponse(obj));
            }
        }).catch((err) => {
            if (onError) {
                onError(checkError(err));
            }
        });
}