﻿function userInfoKill() {
    localStorage.removeItem('user_token');
    localStorage.removeItem('user_firstName');
    localStorage.removeItem('user_lastName');
}

let userToken = localStorage.getItem('user_token');
let userFirstName = localStorage.getItem('user_firstName');
let userLastName = localStorage.getItem('user_lastName');
if (userToken) {
    $('.user-anonymous').css('display', 'none');
    $('.user-authorized').css('display', 'block');
    $('#userLogout').empty();
    $('#userLogout').wrapInner("Hoşgeldiniz," + userFirstName + " " + userLastName + ". [x]");
}
else {
    $('.user-anonymous').css('display', 'block');
    $('.user-authorized').css('display', 'none');
}

$(document).on('click', '#userLogout', function () {
    userInfoKill();
    alert("Oturumunuz sona erdi. Lütfen tekrar giriş yapın.");
    window.location = "/";
});