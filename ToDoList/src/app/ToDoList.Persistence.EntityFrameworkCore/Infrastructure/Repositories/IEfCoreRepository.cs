﻿using Microsoft.EntityFrameworkCore;
using ToDoList.Core.Domain.Entities;
using ToDoList.Core.Domain.Repositories;

namespace ToDoList.Persistence.EntityFrameworkCore.Infrastructure.Repositories
{
    public interface IEfCoreRepository<TDbContext, TEntity> : IRepository<TEntity>
        where TDbContext : DbContext
        where TEntity : class, IEntity
    {
        TDbContext DbContext { get; }

        DbSet<TEntity> DbSet { get; }
    }

    public interface IEfCoreRepository<TDbContext, TEntity, TKey> : IEfCoreRepository<TDbContext, TEntity>, IRepository<TEntity, TKey>
        where TDbContext : DbContext
        where TEntity : class, IEntity<TKey>
    {

    }
}
