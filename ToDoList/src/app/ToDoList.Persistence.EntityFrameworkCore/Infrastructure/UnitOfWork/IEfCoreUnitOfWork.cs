﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data;
using ToDoList.Core.UnitOfWork;

namespace ToDoList.Persistence.EntityFrameworkCore.Infrastructure.UnitOfWork
{
    public interface IEfCoreUnitOfWork<TDbContext> : IUnitOfWork
        where TDbContext : DbContext
    {
        TDbContext DbContext { get; }
        IDbContextTransaction DbContextTransaction { get; }
        IsolationLevel? IsolationLevel { get; }
    }

    public interface IEfCoreUnitOfWork<TDbContext, TEntity, TKey> : IEfCoreUnitOfWork<TDbContext>, IUnitOfWork
        where TDbContext : DbContext
    {
    }
}
