﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDoList.Domain.Todos;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Todos
{
    public class TodoListEntityConfig : IEntityTypeConfiguration<TodoList>
    {
        public void Configure(EntityTypeBuilder<TodoList> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.UserId)
              .IsRequired();

            builder.Property(p => p.Title)
               .IsRequired()
               .HasMaxLength(255);

            builder.Property(p => p.Description)
               .HasMaxLength(500);

            builder.Property(p => p.Status)
               .IsRequired();

            builder.Property(p => p.CreationTime)
               .IsRequired();

            builder.Property(p => p.IsDeleted)
               .IsRequired()
               .HasDefaultValue(false);

            builder.HasOne(q => q.User)
                .WithMany(q => q.TodoLists)
                .HasForeignKey(q => q.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
