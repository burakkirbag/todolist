﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoList.Domain.Todos.Reminders;
using ToDoList.Domain.Todos.Reminders.ComplexTypes;
using ToDoList.Persistence.EntityFrameworkCore.Infrastructure.Repositories;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Reminders
{
    public class TodoReminderRepository : EfCoreRepository<TodoDbContext, TodoReminder, Guid>, ITodoReminderRepository
    {
        public TodoReminderRepository(TodoDbContext dbContext)
            : base(dbContext)
        {
        }

        public List<TodoReminderWithUserAndTodo> GetRemindersWithUserAndTodo()
        {
            DateTimeOffset dtNow = DateTimeOffset.Now;
            var reminders = this
                .Where(q => q.TodoItem.ReminderDate.HasValue && q.TodoItem.ReminderDate <= dtNow)
                .Include(q => q.User)
                .Include(q => q.TodoItem)
                .Select(q => new TodoReminderWithUserAndTodo
                {
                    Id = q.Id,
                    FirstName = q.User.FirstName,
                    LastName = q.User.LastName,
                    Email = q.User.Email,
                    Title = q.TodoItem.Title,
                    Note = q.TodoItem.Note
                })
            .ToList();

            return reminders;
        }
    }
}
