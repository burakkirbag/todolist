﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDoList.Domain.Todos.Reminders;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Reminders
{
    public class TodoReminderEntityConfig : IEntityTypeConfiguration<TodoReminder>
    {
        public void Configure(EntityTypeBuilder<TodoReminder> builder)
        {
            builder.HasKey(q => q.Id);

            builder.Property(q => q.ReminderDate)
                .IsRequired();

            builder.Property(q => q.IsSend)
                .IsRequired()
                .HasDefaultValue(false);

            builder.HasOne(q => q.User)
                .WithMany(q => q.TodoReminders)
                .HasForeignKey(q => q.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(q => q.TodoItem)
                .WithMany(q => q.Reminders)
                .HasForeignKey(q => q.TodoItemId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
