﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDoList.Domain.Todos.Items;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Items
{
    public class TodoItemEntityConfig : IEntityTypeConfiguration<TodoItem>
    {
        public void Configure(EntityTypeBuilder<TodoItem> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.UserId)
              .IsRequired();

            builder.Property(p => p.ListId)
              .IsRequired();

            builder.Property(p => p.Title)
               .IsRequired()
               .HasMaxLength(255);

            builder.Property(p => p.Note)
               .HasMaxLength(500);

            builder.Property(p => p.Status)
               .IsRequired();

            builder.Property(p => p.CreationTime)
               .IsRequired();

            builder.Property(p => p.IsDeleted)
               .IsRequired()
               .HasDefaultValue(false);

            builder.HasOne(q => q.User)
                .WithMany(q => q.TodoItems)
                .HasForeignKey(q => q.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(q => q.List)
                .WithMany(q => q.Items)
                .HasForeignKey(q => q.ListId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
