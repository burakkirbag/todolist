﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;
using ToDoList.Persistence.EntityFrameworkCore.Infrastructure.Repositories;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Items
{
    public class TodoItemRepository : EfCoreRepository<TodoDbContext, TodoItem, Guid>, ITodoItemRepository
    {
        public TodoItemRepository(TodoDbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<int> GetCountByListIdAndUserId(Guid listId, Guid userId)
        {
            var count = await this.CountAsync(q => q.ListId == listId && q.UserId == userId);

            return count;
        }

        public async Task<int> GetActiveStatusCountByListIdAndUserId(Guid listId, Guid userId)
        {
            var count = await this.CountAsync(q => q.ListId == listId && q.UserId == userId && q.Status == TodoStatus.Active);

            return count;
        }

        public async Task<TodoItem> FindByIdAndUserIdAsync(Guid id, Guid userId)
        {
            var todoItem = await this.FirstOrDefaultAsync(q => q.Id == id && q.UserId == userId);

            return todoItem;
        }
    }
}
