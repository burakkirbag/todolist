﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using ToDoList.Domain.Todos;
using ToDoList.Persistence.EntityFrameworkCore.Infrastructure.Repositories;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Todos
{
    public class TodoListRepository : EfCoreRepository<TodoDbContext, TodoList, Guid>, ITodoListRepository
    {
        public TodoListRepository(TodoDbContext dbContext)
            : base(dbContext)
        {
        }

        public async Task<TodoList> FindByIdAndUserIdAsync(Guid id, Guid userId)
        {
            var todoList = await this.FirstOrDefaultAsync(q => q.Id == id && q.UserId == userId);

            return todoList;
        }
    }
}
