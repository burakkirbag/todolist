﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDoList.Domain.Users;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Users
{
    public class UserEntityConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.FirstName)
               .IsRequired()
               .HasMaxLength(50);

            builder.Property(p => p.LastName)
               .IsRequired()
               .HasMaxLength(50);

            builder.Property(p => p.Email)
               .IsRequired()
               .HasMaxLength(100);

            builder.Property(p => p.Password)
               .IsRequired()
               .HasMaxLength(50);

            builder.Property(p => p.RegistrationTime)
               .IsRequired();

            builder.Property(p => p.IsDeleted)
               .IsRequired()
               .HasDefaultValue(false);
        }
    }
}
