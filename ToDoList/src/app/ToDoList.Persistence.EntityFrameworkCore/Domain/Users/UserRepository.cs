﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using ToDoList.Domain.Users;
using ToDoList.Persistence.EntityFrameworkCore.Infrastructure.Repositories;

namespace ToDoList.Persistence.EntityFrameworkCore.Domain.Users
{
    public class UserRepository : EfCoreRepository<TodoDbContext, User, Guid>, IUserRepository
    {
        public UserRepository(TodoDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<User> FindByEmailAndPasswordAsync(string email, string password)
        {
            var findUser = await this.FirstOrDefaultAsync(q => q.Email.Equals(email) && q.Password.Equals(password));

            return findUser;
        }

        public async Task<bool> IsExistsEmailAsync(string email)
        {
            return await this.AnyAsync(q => q.Email.Equals(email));
        }
    }
}
