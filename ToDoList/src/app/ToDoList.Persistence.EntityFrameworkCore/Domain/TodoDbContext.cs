﻿using Microsoft.EntityFrameworkCore;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;
using ToDoList.Domain.Todos.Reminders;
using ToDoList.Domain.Users;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Todos;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Items;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Reminders;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Users;
using ToDoList.Persistence.EntityFrameworkCore.Infrastructure;

namespace ToDoList.Persistence.EntityFrameworkCore
{
    public class TodoDbContext : BaseTodoDbContext
    {
        public TodoDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<User>(new UserEntityConfig());
            modelBuilder.ApplyConfiguration<TodoList>(new TodoListEntityConfig());
            modelBuilder.ApplyConfiguration<TodoItem>(new TodoItemEntityConfig());
            modelBuilder.ApplyConfiguration<TodoReminder>(new TodoReminderEntityConfig());
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<TodoItem> TodoItems { get; set; }
    }
}
