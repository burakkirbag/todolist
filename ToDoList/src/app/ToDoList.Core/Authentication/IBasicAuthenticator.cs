﻿namespace ToDoList.Core.Authentication
{
    public interface IBasicAuthenticator
    {
        TUser Login<TUser>(string userName, string password);
    }
}
