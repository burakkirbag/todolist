﻿using System.Security.Claims;

namespace ToDoList.Core.Authentication
{
    public interface ITokenBasedAuthenticator : IAuthenticator
    {
        string Generate(Claim[] claims);

        bool Validate(string token);
    }
}
