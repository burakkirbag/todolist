﻿namespace ToDoList.Core.BackgroundJobs
{
    public interface IJobBuilder
    {
        void Build();
    }
}
