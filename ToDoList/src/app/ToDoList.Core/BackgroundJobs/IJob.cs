﻿namespace ToDoList.Core.BackgroundJobs
{
    public interface IJob
    {
        void Execute();
    }
}
