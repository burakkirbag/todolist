﻿using System.Threading.Tasks;

namespace ToDoList.Core.Notification.Sms
{
    public interface ISmsSender : INotificationSender
    {
        Task SendAsync(string phoneNumber, string text);
    }
}
