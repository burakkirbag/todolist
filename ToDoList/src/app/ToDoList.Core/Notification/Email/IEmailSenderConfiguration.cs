﻿namespace ToDoList.Core.Notification.Email
{
    public interface IEmailSenderConfiguration
    {
        string DefaultFromAddress { get; set; }

        string DefaultFromDisplayName { get; set; }
    }
}
