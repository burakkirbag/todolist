﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace ToDoList.Core.Notification.Email
{
    public interface IEmailSender : INotificationSender
    {
        Task SendAsync(string to, string subject, string body, bool isBodyHtml = true);

        void Send(string to, string subject, string body, bool isBodyHtml = true);

        Task SendAsync(string from, string to, string subject, string body, bool isBodyHtml = true);

        void Send(string from, string to, string subject, string body, bool isBodyHtml = true);

        Task SendAsync(MailMessage mail, bool normalize = true);

        void Send(MailMessage mail, bool normalize = true);
    }
}
