﻿namespace ToDoList.Core.Notification.Email
{
    public abstract class EmailSenderConfiguration : IEmailSenderConfiguration
    {
        public string DefaultFromAddress { get; set; }

        public string DefaultFromDisplayName { get; set; }
    }
}
