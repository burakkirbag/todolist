﻿namespace ToDoList.Core.Notification.Email.Smtp
{
    public class SmtpEmailSenderConfiguration : EmailSenderConfiguration, ISmtpEmailSenderConfiguration
    {
        public virtual string Host { get; set; }

        public virtual int Port { get; set; }

        public virtual string UserName { get; set; }

        public virtual string Password { get; set; }

        public virtual string Domain { get; set; }

        public virtual bool EnableSsl { get; set; }

        public virtual bool UseDefaultCredentials { get; set; }
    }
}