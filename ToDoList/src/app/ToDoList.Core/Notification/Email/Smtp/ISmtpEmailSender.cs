﻿using System.Net.Mail;

namespace ToDoList.Core.Notification.Email.Smtp
{
    public interface ISmtpEmailSender : IEmailSender
    {
        SmtpClient BuildClient();
    }
}
