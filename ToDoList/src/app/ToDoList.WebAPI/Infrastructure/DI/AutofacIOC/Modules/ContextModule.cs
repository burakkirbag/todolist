﻿using Autofac;
using ToDoList.Application.Common.Context;
using ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Extensions;

namespace ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Modules
{
    public class ContextModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserContext>()
                .As<IUserContext>()
                .InstancePerAspNetCoreRequest();
        }
    }
}
