﻿using Autofac;
using ToDoList.Core.UnitOfWork;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;
using ToDoList.Domain.Todos.Reminders;
using ToDoList.Domain.Users;
using ToDoList.Persistence.EntityFrameworkCore;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Todos;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Items;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Todos.Reminders;
using ToDoList.Persistence.EntityFrameworkCore.Domain.Users;
using ToDoList.Persistence.EntityFrameworkCore.Infrastructure.UnitOfWork;
using ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Extensions;

namespace ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Modules
{
    public class DomainModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserRepository>()
                .As<IUserRepository>()
                .InstancePerAspNetCoreRequest();

            builder.RegisterType<TodoListRepository>()
                .As<ITodoListRepository>()
                .InstancePerAspNetCoreRequest();

            builder.RegisterType<TodoItemRepository>()
                .As<ITodoItemRepository>()
                .InstancePerAspNetCoreRequest();

            builder.RegisterType<TodoReminderRepository>()
               .As<ITodoReminderRepository>()
               .InstancePerAspNetCoreRequest();

            builder.RegisterType<EfCoreUnitOfWork<TodoDbContext>>()
                .As<IUnitOfWork>()
                .InstancePerAspNetCoreRequest();
        }
    }
}
