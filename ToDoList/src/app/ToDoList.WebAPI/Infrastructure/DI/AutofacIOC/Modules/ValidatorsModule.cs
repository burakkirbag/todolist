﻿using Autofac;
using System.Linq;
using System.Reflection;
using ToDoList.Application.Common.Validation;
using ToDoList.Application.Users.Commands.CreateUser;
using ToDoList.Core.Utilities.Extensions;

namespace ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Modules
{
    public class ValidatorsModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly servicesAssembly = typeof(CreateUserCommandValidator).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(servicesAssembly)
                .Where(x => x.IsSubclassOfRawGeneric(typeof(ValidationBehaviorBase<,>)))
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}
