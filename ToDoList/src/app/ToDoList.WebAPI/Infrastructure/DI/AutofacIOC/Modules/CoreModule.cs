﻿using Autofac;
using ToDoList.Core.Notification.Email.Smtp;
using ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Extensions;

namespace ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Modules
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SmtpEmailSenderConfiguration>()
                .As<ISmtpEmailSenderConfiguration>()
                .SingleInstance();

            builder.RegisterType<SmtpEmailSender>()
               .As<ISmtpEmailSender>()
               .InstancePerAspNetCoreRequest();
        }
    }
}
