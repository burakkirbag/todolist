﻿using Autofac;
using ToDoList.Core.Authentication;
using ToDoList.Core.BackgroundJobs;
using ToDoList.Infrastructure.Authentication.Jwt;
using ToDoList.Infrastructure.BackgroundJobs.HangFire;
using ToDoList.Infrastructure.BackgroundJobs.HangFire.Jobs;
using ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Extensions;

namespace ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Modules
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<JwtAuthenticator>()
                .As<ITokenBasedAuthenticator>()
                .InstancePerAspNetCoreRequest();

            builder.RegisterType<HangfireJobBuilder>()
                .As<IJobBuilder>()
                .InstancePerAspNetCoreRequest();

            builder.RegisterType<EmailNotificationJob>()
               .As<IJob>()
               .InstancePerAspNetCoreRequest();
        }
    }
}
