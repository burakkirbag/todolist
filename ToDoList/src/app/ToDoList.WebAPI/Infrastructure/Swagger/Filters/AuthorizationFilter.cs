﻿using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ToDoList.WebAPI.Infrastructure.Swagger.Filters
{
    public class AuthorizationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            bool requiresAuthorization = false;

            MethodInfo mi;
            context.ApiDescription.TryGetMethodInfo(out mi);

            AuthorizeAttribute authorizeAttribute = mi.GetCustomAttribute<AuthorizeAttribute>();

            if (authorizeAttribute != null)
            {
                requiresAuthorization = true;
            }
            else
            {
                bool methodAllowsAnonymous = mi.GetCustomAttributes<AllowAnonymousAttribute>().Any();

                if (!methodAllowsAnonymous)
                    requiresAuthorization = true;
            }

            if (!requiresAuthorization)
                return;
            
            operation.Responses.Add("401", new Response { Description = "Unauthorized" });

            if (!string.IsNullOrEmpty(authorizeAttribute.Policy) || !string.IsNullOrEmpty(authorizeAttribute.Roles))
                operation.Responses.Add("403", new Response { Description = "Access Forbidden" });

            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            operation.Parameters.Add(new NonBodyParameter()
            {
                Name = "Authorization",
                In = "header",
                Type = "string",
                Required = true
            });
        }
    }
}
