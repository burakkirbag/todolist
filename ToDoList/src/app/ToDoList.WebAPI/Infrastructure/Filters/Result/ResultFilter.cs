﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using ToDoList.Application.Common.Messages;

namespace ToDoList.WebAPI.Infrastructure.Filters.Result
{
    public class ResultFilter : IResultFilter
    {
        public void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Result is ObjectResult objectResult)
            {
                ApiResult apiResult = new ApiResult
                {
                    Success = true,
                    Code = (int)HttpStatusCode.OK
                };

                if (objectResult.Value is BaseCommandResult commandResult)
                {
                    apiResult.Message = commandResult.Message;
                    apiResult.InternalMessage = commandResult.InternalMessage;
                }
                else
                {
                    apiResult.Data = objectResult.Value;
                }

                objectResult.Value = apiResult;
            }
        }

        public void OnResultExecuted(ResultExecutedContext context)
        {
        }
    }
}
