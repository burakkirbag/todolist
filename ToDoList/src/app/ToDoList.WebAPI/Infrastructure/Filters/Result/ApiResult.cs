﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ToDoList.WebAPI.Infrastructure.Filters.Result
{
    /// <summary>
    /// Web Api Cevabı
    /// </summary>
    [DataContract]
    public class ApiResult
    {
        /// <summary>
        /// Başarılı
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Http Durum Kodu
        /// </summary>
        [DataMember]
        public int Code { get; set; }

        /// <summary>
        /// Mesaj
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string Message { get; set; }

        /// <summary>
        /// Mesaj
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public string InternalMessage { get; set; }

        /// <summary>
        /// Veri
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public object Data { get; set; }

        /// <summary>
        /// Validasyon Mesajları
        /// </summary>
        [DataMember(EmitDefaultValue = false)]
        public List<ApiResultValidationMessage> Validations { get; set; }
    }
}
