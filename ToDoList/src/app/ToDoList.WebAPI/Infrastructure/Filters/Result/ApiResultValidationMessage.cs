﻿namespace ToDoList.WebAPI.Infrastructure.Filters.Result
{
    public class ApiResultValidationMessage
    {
        public string Field { get; set; }
        public string Message { get; set; }

        public ApiResultValidationMessage(string field, string message)
        {
            Field = field;
            Message = message;
        }
    }
}
