﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Net;
using ToDoList.Application.Authentication.GetUserToken.Exceptions;
using ToDoList.WebAPI.Infrastructure.Filters.Result;

namespace ToDoList.WebAPI.Infrastructure.Filters.Exception
{
    public class ExceptionFilter : IExceptionFilter
    {
        public ExceptionFilter()
        {
        }

        public void OnException(ExceptionContext context)
        {
            HandleAndWrapException(context);
        }

        private void HandleAndWrapException(ExceptionContext context)
        {
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;

            ApiResult apiResult = new ApiResult
            {
                Success = false,
                Code = GetCode(context)
            };

            if (context.Exception is ValidationException validationException)
            {
                apiResult.Message = "Lütfen girmiş olduğunuz bilgileri kontrol ederek, tekrar deneyin.";

                var apiResultValidations = new List<ApiResultValidationMessage>();
                foreach (var error in validationException.Errors)
                {
                    apiResultValidations.Add(new ApiResultValidationMessage(error.PropertyName, error.ErrorMessage));
                }

                if (apiResultValidations.Count > 0)
                {
                    apiResult.Validations = apiResultValidations;
                }
            }
            else
            {
                apiResult.Message = context.Exception.Message;
                apiResult.InternalMessage = context.Exception.StackTrace;
            }

            context.Result = new ObjectResult(apiResult);

            context.Exception = null; //Handled!
            context.ExceptionHandled = true;
        }

        private int GetCode(ExceptionContext context)
        {
            if (context.Exception is LoginFailedException)
            {
                return (int)HttpStatusCode.Unauthorized;
            }

            if (context.Exception is ValidationException)
            {
                return (int)HttpStatusCode.BadRequest;
            }

            if (context.Exception is SecurityTokenExpiredException)
            {
                return (int)HttpStatusCode.Unauthorized;
            }

            if (context.Exception is SecurityTokenValidationException)
            {
                return (int)HttpStatusCode.Unauthorized;
            }

            if (context.Exception is SecurityTokenException)
            {
                return (int)HttpStatusCode.Unauthorized;
            }

            return (int)HttpStatusCode.InternalServerError;
        }
    }
}
