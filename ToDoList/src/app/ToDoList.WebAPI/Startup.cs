using Autofac;
using Autofac.Extensions.DependencyInjection;
using Hangfire;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Application.Users.Commands.CreateUser;
using ToDoList.Core.BackgroundJobs;
using ToDoList.Infrastructure.Authentication.Jwt;
using ToDoList.Infrastructure.Logger;
using ToDoList.Persistence.EntityFrameworkCore;
using ToDoList.WebAPI.Infrastructure.DI.AutofacIOC.Modules;
using ToDoList.WebAPI.Infrastructure.Filters.Exception;
using ToDoList.WebAPI.Infrastructure.Filters.Result;
using ToDoList.WebAPI.Infrastructure.Swagger.Filters;

namespace ToDoList.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public AutofacServiceProvider AutofacProvider { get; set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<JwtAuthSettings>(Configuration.GetSection("JwtAuthSettings"));

            services.AddMvc();
            services.AddCors();

            services.AddMvcCore(options =>
            {
                options.Filters.Add(new ResultFilter());
                options.Filters.Add(typeof(ExceptionFilter));
            })
             .AddJsonOptions(options =>
             {
                 options.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
                 options.SerializerSettings.DateFormatString = "dd.MM.yyyy HH:mm:ss";
                 options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
             });

            services.AddDbContext<TodoDbContext>(optionsBuilder => optionsBuilder.UseSqlServer(Configuration.GetConnectionString(nameof(TodoDbContext))));

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateAudience = true,
                    ValidAudience = Configuration["JwtAuthSettings:Audience"],
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["JwtAuthSettings:Issuer"],
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtAuthSettings:SecurityKey"]))
                };

                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = ctx =>
                    {
                        return Task.CompletedTask;
                    },
                    OnAuthenticationFailed = ctx =>
                    {
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "TodoList Api", Version = "v1" });

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(security);

                var webAPIXmlPath = Path.Combine(AppContext.BaseDirectory, "ToDoList.WebAPI.xml");
                var applicationXmlPath = Path.Combine(AppContext.BaseDirectory, "ToDoList.Application.xml");

                c.IncludeXmlComments(webAPIXmlPath);
                c.IncludeXmlComments(applicationXmlPath);

                c.DescribeAllEnumsAsStrings();

                c.OperationFilter<AuthorizationFilter>();
            });

            services.AddMediatR(typeof(CreateUserCommand).Assembly);

            services.AddHttpContextAccessor();

            services.AddHangfire(_ => _.UseSqlServerStorage(Configuration.GetConnectionString(nameof(TodoDbContext))));

            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule<CoreModule>();
            containerBuilder.RegisterModule<ContextModule>();
            containerBuilder.RegisterModule<DomainModule>();
            containerBuilder.RegisterModule<ValidatorsModule>();
            containerBuilder.RegisterModule<InfrastructureModule>();

            containerBuilder.Populate(services);

            var container = containerBuilder.Build();
            var serviceProvider = new AutofacServiceProvider(container);

            AutofacProvider = serviceProvider;

            return serviceProvider;

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {           
            app.UseAuthentication().UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseMvc();
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "TodoList API v1");

                c.DocumentTitle = "TodoList API v1";
                c.DocExpansion(DocExpansion.None);
            });

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            loggerFactory.AddProvider(new FileLogProvider());

            AutofacProvider.GetService<IJobBuilder>().Build();            
        }
    }
}
