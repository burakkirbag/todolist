﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using ToDoList.Application.Users.Commands.CreateUser;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoList.WebAPI.Controllers
{
    /// <summary>
    /// Kullanıcı Kayıt
    /// </summary>
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<AccountController> _logger;

        /// <summary>
        /// Kullanıcı Kayıt
        /// </summary>
        /// <param name="mediator"></param>
        public AccountController(IMediator mediator, ILogger<AccountController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// Kullanıcı Kayıt
        /// </summary>
        /// <param name="value"></param>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateUserCommand value)
        {
            try
            {
                var user = await _mediator.Send(value);

                return Ok(user);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
