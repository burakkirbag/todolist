﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using ToDoList.Application.Authentication.Commands.GetUserToken;

namespace ToDoList.WebAPI.Controllers
{
    /// <summary>
    /// Kimlik Doğrulama
    /// </summary>
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<AuthController> _logger;

        /// <summary>
        /// Kimlik Doğrulama
        /// </summary>
        /// <param name="mediator"></param>
        public AuthController(IMediator mediator, ILogger<AuthController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// E-Posta ve Şifre ile Kimlik Doğrula
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]GetUserTokenCommand value)
        {
            try
            {
                var result = await _mediator.Send(value);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
