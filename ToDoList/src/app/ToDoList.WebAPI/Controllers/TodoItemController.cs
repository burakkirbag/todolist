﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using ToDoList.Application.TodoItems.Commands.CreateTodoItem;
using ToDoList.Application.TodoItems.Commands.DeleteTodoItem;
using ToDoList.Application.TodoItems.Commands.UpdateTodoItem;
using ToDoList.Application.TodoItems.Queries.GetTodoItem;
using ToDoList.Application.TodoItems.Queries.GetTodoItems;

namespace ToDoList.WebAPI.Controllers
{
    /// <summary>
    /// Görevler
    /// </summary>
    [Route("api/[controller]")]
    public class TodoItemController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<TodoItemController> _logger;

        /// <summary>
        /// Görevler
        /// </summary>
        /// <param name="mediator"></param>
        public TodoItemController(IMediator mediator, ILogger<TodoItemController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// Görevleri Listele
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]string listId)
        {
            try
            {
                var result = await _mediator.Send(new GetTodoItemsQuery { ListId = Guid.Parse(listId) });

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Görev Görüntüle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var result = await _mediator.Send(new GetTodoItemQuery { Id = id });

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Görev Ekle
        /// </summary>
        /// <param name="value"></param>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateTodoItemCommand value)
        {
            try
            {
                var result = await _mediator.Send(value);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Görev Düzenle
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]UpdateTodoItemCommand value)
        {
            try
            {
                var result = await _mediator.Send(value);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Görev Sil
        /// </summary>
        /// <param name="id"></param>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                var result = await _mediator.Send(new DeleteTodoItemCommand { Id = Guid.Parse(id) });

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
