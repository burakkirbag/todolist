﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using ToDoList.Application.TodoLists.Commands.CreateTodoList;
using ToDoList.Application.TodoLists.Commands.DeleteTodoList;
using ToDoList.Application.TodoLists.Commands.UpdateTodoList;
using ToDoList.Application.TodoLists.Queries.GetTodoList;
using ToDoList.Application.TodoLists.Queries.GetTodoLists;

namespace ToDoList.WebAPI.Controllers
{
    /// <summary>
    /// Görev Listesi
    /// </summary>
    [Route("api/[controller]")]
    public class TodoListController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly ILogger<TodoListController> _logger;

        /// <summary>
        /// Görev Listesi
        /// </summary>
        /// <param name="mediator"></param>
        public TodoListController(IMediator mediator, ILogger<TodoListController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// Listeleri Görüntüle
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = await _mediator.Send(new GetTodoListsQuery { });
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Listeyi Görüntüle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {

                var result = await _mediator.Send(new GetTodoListQuery { Id = id });

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Liste Ekle
        /// </summary>
        /// <param name="value"></param>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateTodoListCommand value)
        {
            try
            {
                var result = await _mediator.Send(value);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Listeyi Düzenle
        /// </summary>
        /// <param name="id">Liste Id</param>
        /// <param name="value">Liste Bilgisi</param>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]UpdateTodoListCommand value)
        {
            try
            {
                var result = await _mediator.Send(value);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Listeyi Sil
        /// </summary>
        /// <param name="id"></param>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                var result = await _mediator.Send(new DeleteTodoListCommand { Id = Guid.Parse(id) });

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
