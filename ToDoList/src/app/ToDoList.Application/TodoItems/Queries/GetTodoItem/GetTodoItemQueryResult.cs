﻿namespace ToDoList.Application.TodoItems.Queries.GetTodoItem
{
    public class GetTodoItemQueryResult
    {
        public TodoItemDto TodoItem { get; set; }
    }
}
