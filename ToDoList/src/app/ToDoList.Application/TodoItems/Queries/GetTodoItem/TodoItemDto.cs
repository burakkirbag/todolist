﻿using System;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoItems.Queries.GetTodoItem
{
    public class TodoItemDto
    {
        /// <summary>
        /// Liste Id
        /// </summary>
        public Guid ListId { get; set; }

        /// <summary>
        /// Liste Başlık
        /// </summary>
        public string ListTitle { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Başlık
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Not
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Bitiş Tarihi
        /// </summary>
        public DateTimeOffset? DueDate { get; set; }

        /// <summary>
        /// Hatırlatma Tarihi
        /// </summary>
        public DateTimeOffset? ReminderDate { get; set; }

        /// <summary>
        /// Durumu
        /// </summary>
        public TodoStatus Status { get; set; }

        /// <summary>
        /// Durumu
        /// </summary>
        public string StatusText { get; set; }

        /// <summary>
        /// Oluşturulma Zamanı
        /// </summary>
        public DateTimeOffset CreationTime { get; set; }
    }
}
