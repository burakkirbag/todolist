﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoItems.Queries.GetTodoItem.Exceptions;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;

namespace ToDoList.Application.TodoItems.Queries.GetTodoItem
{
    public class GetTodoItemQueryHandler : IRequestHandler<GetTodoItemQuery, GetTodoItemQueryResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoItemRepository _todoItemRepository;
        private readonly IUserContext _userContext;

        public GetTodoItemQueryHandler(IMediator mediator, ITodoItemRepository todoItemRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoItemRepository = todoItemRepository;
            _userContext = userContext;
        }

        public async Task<GetTodoItemQueryResult> Handle(GetTodoItemQuery request, CancellationToken cancellationToken)
        {
            var result = new GetTodoItemQueryResult();

            var todoItem = await _todoItemRepository
                .Where(q => q.Id.Equals(request.Id) && q.UserId.Equals(_userContext.Id))
                .Include(q => q.List)
                .Select(q => new TodoItemDto
                {
                    ListId = q.ListId,
                    ListTitle = q.List.Title,
                    Id = q.Id,
                    Title = q.Title,
                    Note = q.Note,
                    DueDate = q.DueDate,
                    ReminderDate = q.ReminderDate,
                    Status = q.Status,
                    StatusText = (q.Status == TodoStatus.Completed ? "Tamamlandı" : "Aktif"),
                    CreationTime = q.CreationTime
                })
                .FirstOrDefaultAsync();

            if (todoItem == null)
                throw new GetTodoItemNotFoundException("Kayıt bulunamadı.");

            result.TodoItem = todoItem;

            return result;
        }
    }
}
