﻿using MediatR;
using System;

namespace ToDoList.Application.TodoItems.Queries.GetTodoItem
{
    public class GetTodoItemQuery : IRequest<GetTodoItemQueryResult>
    {
        public Guid Id { get; set; }
    }
}
