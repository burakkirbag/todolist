﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.TodoItems.Queries.GetTodoItem.Exceptions
{
    public class GetTodoItemNotFoundException : Exception
    {
        public GetTodoItemNotFoundException()
        {
        }

        public GetTodoItemNotFoundException(string message) : base(message)
        {
        }

        public GetTodoItemNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GetTodoItemNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
