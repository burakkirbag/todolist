﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoItems.Queries.GetTodoItem;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;

namespace ToDoList.Application.TodoItems.Queries.GetTodoItems
{
    public class GetTodoItemsQueryHandler : IRequestHandler<GetTodoItemsQuery, GetTodoItemsQueryResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoItemRepository _todoItemRepository;
        private readonly IUserContext _userContext;

        public GetTodoItemsQueryHandler(IMediator mediator, ITodoItemRepository todoItemRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoItemRepository = todoItemRepository;
            _userContext = userContext;
        }

        public async Task<GetTodoItemsQueryResult> Handle(GetTodoItemsQuery request, CancellationToken cancellationToken)
        {
            var result = new GetTodoItemsQueryResult();

            var todoItems = await _todoItemRepository
                .Where(q => q.ListId.Equals(request.ListId) && q.UserId.Equals(_userContext.Id))
                .Include(q => q.List)
                .Select(q => new TodoItemDto
                {
                    ListId = q.ListId,
                    ListTitle = q.List.Title,
                    Id = q.Id,
                    Title = q.Title,
                    Note = q.Note,
                    DueDate = q.DueDate,
                    ReminderDate = q.ReminderDate,
                    Status = q.Status,
                    StatusText = (q.Status == TodoStatus.Completed ? "Tamamlandı" : "Aktif"),
                    CreationTime = q.CreationTime
                })
                .ToListAsync();

            if (todoItems.Count > 0)
            {
                result.TodoItems = todoItems;
            }

            return result;
        }
    }
}
