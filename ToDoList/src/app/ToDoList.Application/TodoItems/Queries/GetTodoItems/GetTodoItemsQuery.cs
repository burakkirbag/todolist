﻿using MediatR;
using System;

namespace ToDoList.Application.TodoItems.Queries.GetTodoItems
{
    public class GetTodoItemsQuery : IRequest<GetTodoItemsQueryResult>
    {
        public Guid ListId { get; set; }
    }
}
