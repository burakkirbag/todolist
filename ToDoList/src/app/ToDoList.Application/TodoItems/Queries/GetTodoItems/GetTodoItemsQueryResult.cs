﻿using System.Collections.Generic;
using ToDoList.Application.TodoItems.Queries.GetTodoItem;

namespace ToDoList.Application.TodoItems.Queries.GetTodoItems
{
    public class GetTodoItemsQueryResult
    {
        public List<TodoItemDto> TodoItems { get; set; }
    }
}
