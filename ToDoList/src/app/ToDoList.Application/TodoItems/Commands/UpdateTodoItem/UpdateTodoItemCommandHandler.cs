﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoItems.Commands.UpdateTodoItem.Exceptions;
using ToDoList.Application.TodoReminders.Commands.CreateTodoReminder;
using ToDoList.Domain.Todos.Items;

namespace ToDoList.Application.TodoItems.Commands.UpdateTodoItem
{
    /// <summary>
    /// Görev Düzenleme
    /// </summary>
    public class UpdateTodoItemCommandHandler : IRequestHandler<UpdateTodoItemCommand, UpdateTodoItemCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoItemRepository _todoItemRepository;
        private readonly IUserContext _userContext;

        // <summary>
        /// Görev Düzenleme
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoListRepository"></param>
        /// <param name="userContext"></param>
        public UpdateTodoItemCommandHandler(IMediator mediator, ITodoItemRepository todoItemRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoItemRepository = todoItemRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpdateTodoItemCommandResult> Handle(UpdateTodoItemCommand request, CancellationToken cancellationToken)
        {
            var todoItem = await _todoItemRepository.FindByIdAndUserIdAsync(request.Id, _userContext.Id);

            if (todoItem == null)
                throw new UpdateTodoItemNotFoundException("Düzenlemek istediğiniz kayıt bulunamadı.");

            bool isAddReminder = false;
            if (!todoItem.ReminderDate.Equals(request.ReminderDate))
                isAddReminder = true;

            todoItem.Title = request.Title;
            todoItem.Note = request.Note;
            todoItem.DueDate = request.DueDate;
            todoItem.ReminderDate = request.ReminderDate;
            todoItem.Status = request.Status;

            var updateEntity = await _todoItemRepository.UpdateAsync(todoItem, true, cancellationToken);

            if (isAddReminder)
            {
                await _mediator.Send(new CreateTodoReminderCommand { TodoItemId = todoItem.Id });
            }

            return new UpdateTodoItemCommandResult { Message = "Görev güncellenmiştir." };
        }
    }
}
