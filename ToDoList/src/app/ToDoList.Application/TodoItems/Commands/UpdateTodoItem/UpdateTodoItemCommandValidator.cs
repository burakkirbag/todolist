﻿using FluentValidation;
using System;

namespace ToDoList.Application.TodoItems.Commands.UpdateTodoItem
{
    /// <summary>
    /// Görev Düzenleme Validasyonu
    /// </summary>
    public class UpdateTodoItemCommandValidator : AbstractValidator<UpdateTodoItemCommand>
    {
        /// <summary>
        /// Görev Düzenleme Validasyonu
        /// </summary>
        public UpdateTodoItemCommandValidator()
        {
            RuleFor(r => r.Id)
                .NotNull()
                .NotEmpty()
                .NotEqual(Guid.Empty).WithMessage("Düzenlenecek kayıt bilgisi boş olamaz.");

            RuleFor(r => r.Title)
               .NotNull()
               .NotEmpty().WithMessage("Başlık girmelisiniz.")
               .MaximumLength(100).WithMessage("Başlık en fazla 100 karakter olabilir.");

            RuleFor(r => r.Note)
                .MaximumLength(500).WithMessage("Not en fazla 500 karakter olabilir.");

            RuleFor(r => r.Status)
                .NotNull()
                .NotEmpty().WithMessage("Durum bilgisi boş olamaz.");
        }
    }
}
