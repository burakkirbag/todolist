﻿using MediatR;
using System;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoItems.Commands.UpdateTodoItem
{
    /// <summary>
    /// Görev Düzenleme İsteği
    /// </summary>
    public class UpdateTodoItemCommand : IRequest<UpdateTodoItemCommandResult>
    {
        /// <summary>
        /// Görev Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Başlık
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Not
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Bitiş Tarihi
        /// </summary>
        public DateTimeOffset? DueDate { get; set; }

        /// <summary>
        /// Hatırlatma Tarihi
        /// </summary>
        public DateTimeOffset? ReminderDate { get; set; }

        /// <summary>
        /// Durumu
        /// </summary>
        public TodoStatus Status { get; set; }
    }
}
