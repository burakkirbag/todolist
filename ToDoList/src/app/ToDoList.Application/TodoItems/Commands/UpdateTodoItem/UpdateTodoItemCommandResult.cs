﻿using ToDoList.Application.Common.Messages;

namespace ToDoList.Application.TodoItems.Commands.UpdateTodoItem
{
    /// <summary>
    /// Görev Düzenleme Cevabı
    /// </summary>
    public class UpdateTodoItemCommandResult : BaseCommandResult
    {
    }
}