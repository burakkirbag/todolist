﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.TodoItems.Commands.UpdateTodoItem.Exceptions
{
    public class UpdateTodoItemNotFoundException : Exception
    {
        public UpdateTodoItemNotFoundException()
        {
        }

        public UpdateTodoItemNotFoundException(string message) : base(message)
        {
        }

        public UpdateTodoItemNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UpdateTodoItemNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
