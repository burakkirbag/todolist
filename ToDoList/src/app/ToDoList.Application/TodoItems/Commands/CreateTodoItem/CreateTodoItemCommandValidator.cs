﻿using FluentValidation;
using System;
using ToDoList.Application.Common.Validation;

namespace ToDoList.Application.TodoItems.Commands.CreateTodoItem
{
    /// <summary>
    /// Görev Token Olusturma Validasyon
    /// </summary>
    public class CreateTodoItemCommandValidatorPipeLine : ValidationBehavior<CreateTodoItemCommand, CreateTodoItemCommandResult>
    {
        public CreateTodoItemCommandValidatorPipeLine() : base(new CreateTodoItemCommandValidator()) { }
    }

    /// <summary>
    /// Görev Oluşturma Validasyonu
    /// </summary>
    public class CreateTodoItemCommandValidator : AbstractValidator<CreateTodoItemCommand>
    {
        /// <summary>
        /// Görev Oluşturma Validasyonu
        /// </summary>
        public CreateTodoItemCommandValidator()
        {
            RuleFor(r => r.ListId)
                .NotNull()
                .NotEmpty()
                .NotEqual(Guid.Empty).WithMessage("Görevin ekleneceği liste bilgisi bulunamadı.");

            RuleFor(r => r.Title)
               .NotNull()
               .NotEmpty().WithMessage("Başlık girmelisiniz.")
               .MaximumLength(100).WithMessage("Başlık en fazla 100 karakter olabilir.");

            RuleFor(r => r.Note)
                .MaximumLength(1000).WithMessage("Notunuz en fazla 500 karakter olabilir.");
        }
    }
}
