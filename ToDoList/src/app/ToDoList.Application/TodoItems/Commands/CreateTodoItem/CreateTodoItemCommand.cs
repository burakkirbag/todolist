﻿using MediatR;
using System;

namespace ToDoList.Application.TodoItems.Commands.CreateTodoItem
{
    /// <summary>
    /// Görev Oluşturma İsteği
    /// </summary>
    public class CreateTodoItemCommand : IRequest<CreateTodoItemCommandResult>
    {
        /// <summary>
        /// Liste Id
        /// </summary>
        public Guid ListId { get; set; }

        /// <summary>
        /// Başlık
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Not
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Bitiş Tarihi
        /// </summary>
        public DateTimeOffset? DueDate { get; set; }

        /// <summary>
        /// Hatırlatma Tarihi
        /// </summary>
        public DateTimeOffset? ReminderDate { get; set; }
    }
}
