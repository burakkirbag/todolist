﻿using ToDoList.Application.Common.Messages;

namespace ToDoList.Application.TodoItems.Commands.CreateTodoItem
{
    /// <summary>
    /// Görev Oluşturma Cevabı
    /// </summary>
    public class CreateTodoItemCommandResult : BaseCommandResult
    {
    }
}
