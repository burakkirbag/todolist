﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoReminders.Commands.CreateTodoReminder;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;

namespace ToDoList.Application.TodoItems.Commands.CreateTodoItem
{
    /// <summary>
    /// Görev Oluşturma
    /// </summary>
    public class CreateTodoItemCommandHandler : IRequestHandler<CreateTodoItemCommand, CreateTodoItemCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoItemRepository _todoItemRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Görev Oluşturma
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoItemRepository"></param>
        /// <param name="userContext"></param>
        public CreateTodoItemCommandHandler(IMediator mediator, ITodoItemRepository todoItemRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoItemRepository = todoItemRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateTodoItemCommandResult> Handle(CreateTodoItemCommand request, CancellationToken cancellationToken)
        {
            var entity = new TodoItem
            {
                UserId = _userContext.Id,
                ListId = request.ListId,
                Title = request.Title,
                Note = request.Note,
                DueDate = request.DueDate,
                ReminderDate = request.ReminderDate,
                Status = TodoStatus.Active,
                CreationTime = DateTimeOffset.Now
            };

            var todoItem = await _todoItemRepository.InsertAsync(entity, true, cancellationToken);

            if (request.ReminderDate.HasValue)
            {
                await _mediator.Send(new CreateTodoReminderCommand { TodoItemId = todoItem.Id });
            }

            return new CreateTodoItemCommandResult { Message = "Yeni görev eklenmiştir." };
        }
    }
}
