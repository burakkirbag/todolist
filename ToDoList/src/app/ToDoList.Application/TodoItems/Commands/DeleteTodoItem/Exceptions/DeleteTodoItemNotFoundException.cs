﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.TodoItems.Commands.DeleteTodoItem.Exceptions
{
    public class DeleteTodoItemNotFoundException : Exception
    {
        public DeleteTodoItemNotFoundException()
        {
        }

        public DeleteTodoItemNotFoundException(string message) : base(message)
        {
        }

        public DeleteTodoItemNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DeleteTodoItemNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
