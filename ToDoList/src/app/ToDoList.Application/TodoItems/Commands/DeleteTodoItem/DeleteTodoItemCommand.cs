﻿using MediatR;
using System;

namespace ToDoList.Application.TodoItems.Commands.DeleteTodoItem
{
    /// <summary>
    /// Görev Silme İsteği
    /// </summary>
    public class DeleteTodoItemCommand : IRequest<DeleteTodoItemCommandResult>
    {
        /// <summary>
        /// Görev Id
        /// </summary>
        public Guid Id { get; set; }
    }
}
