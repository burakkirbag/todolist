﻿using FluentValidation;
using System;
using ToDoList.Application.Common.Validation;

namespace ToDoList.Application.TodoItems.Commands.DeleteTodoItem
{
    /// <summary>
    /// Görev Silme Validasyonu
    /// </summary>
    public class DeleteTodoItemCommandValidatorPipeLine : ValidationBehavior<DeleteTodoItemCommand, DeleteTodoItemCommandResult>
    {
        public DeleteTodoItemCommandValidatorPipeLine() : base(new DeleteTodoItemCommandValidator()) { }
    }

    /// <summary>
    /// Görev Silme Validasyonu
    /// </summary>
    public class DeleteTodoItemCommandValidator : AbstractValidator<DeleteTodoItemCommand>
    {
        /// <summary>
        /// Görev Silme Validasyonu
        /// </summary>
        public DeleteTodoItemCommandValidator()
        {
            RuleFor(r => r.Id)
                .NotNull()
                .NotEmpty()
                .NotEqual(Guid.Empty).WithMessage("Silinecek kayıt bilgisi boş olamaz.");
        }
    }
}
