﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoItems.Commands.DeleteTodoItem.Exceptions;
using ToDoList.Application.TodoReminders.Commands.DeleteTodoReminder;
using ToDoList.Domain.Todos.Items;

namespace ToDoList.Application.TodoItems.Commands.DeleteTodoItem
{
    /// <summary>
    /// Görev Silme
    /// </summary>
    public class DeleteTodoItemCommandHandler : IRequestHandler<DeleteTodoItemCommand, DeleteTodoItemCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoItemRepository _todoItemRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Görev Silme
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoItemRepository"></param>
        /// <param name="userContext"></param>
        public DeleteTodoItemCommandHandler(IMediator mediator, ITodoItemRepository todoItemRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoItemRepository = todoItemRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DeleteTodoItemCommandResult> Handle(DeleteTodoItemCommand request, CancellationToken cancellationToken)
        {
            var todoItem = await _todoItemRepository.FindByIdAndUserIdAsync(request.Id, _userContext.Id);

            if (todoItem == null)
                throw new DeleteTodoItemNotFoundException("Silmek istediğiniz kayıt bulunamadı.");

            await _todoItemRepository.DeleteAsync(request.Id, true, cancellationToken);

            await _mediator.Send(new DeleteTodoReminderCommand { TodoId = request.Id });

            return new DeleteTodoItemCommandResult { Message = "Görev silinmiştir." };
        }
    }
}
