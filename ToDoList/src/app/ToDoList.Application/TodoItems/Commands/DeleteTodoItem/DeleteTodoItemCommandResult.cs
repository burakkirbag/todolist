﻿using ToDoList.Application.Common.Messages;

namespace ToDoList.Application.TodoItems.Commands.DeleteTodoItem
{
    /// <summary>
    /// Görev Silme İsteği Cevabı
    /// </summary>
    public class DeleteTodoItemCommandResult : BaseCommandResult
    {
    }
}
