﻿using System;

namespace ToDoList.Application.Common.Context
{
    /// <summary>
    /// Kullanıcı Bilgisi
    /// </summary>
    public interface IUserContext
    {
        /// <summary>
        /// Kullanıcı Id
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Adı
        /// </summary>
        string FirstName { get; }

        /// <summary>
        /// Soyadı
        /// </summary>
        string LastName { get; }

        /// <summary>
        /// E-Posta Adresi
        /// </summary>
        string Email { get; }
    }
}
