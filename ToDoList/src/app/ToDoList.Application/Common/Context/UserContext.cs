﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace ToDoList.Application.Common.Context
{
    /// <summary>
    /// Kullanıcı Bilgisi
    /// </summary>
    public class UserContext : IUserContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Kullanıcı Bilgisi
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public UserContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Kullanıcı Id
        /// </summary>
        public Guid Id
        {
            get
            {
                var claim = ((ClaimsIdentity)_httpContextAccessor.HttpContext.User.Identity).FindFirst(ClaimTypes.NameIdentifier);

                var value = claim?.Value;

                if (string.IsNullOrEmpty(value))
                    return Guid.Empty;
                else
                    return Guid.Parse(value);
            }
        }

        /// <summary>
        /// Adı
        /// </summary>
        public string FirstName
        {
            get
            {
                var claim = ((ClaimsIdentity)_httpContextAccessor.HttpContext.User.Identity).FindFirst(ClaimTypes.Name);

                var value = claim?.Value;

                return value;
            }
        }

        /// <summary>
        /// Soyadı
        /// </summary>
        public string LastName
        {
            get
            {
                var claim = ((ClaimsIdentity)_httpContextAccessor.HttpContext.User.Identity).FindFirst(ClaimTypes.GivenName);

                var value = claim?.Value;

                return value;
            }
        }

        /// <summary>
        /// E-Posta Adresi
        /// </summary>
        public string Email
        {
            get
            {
                var claim = ((ClaimsIdentity)_httpContextAccessor.HttpContext.User.Identity).FindFirst(ClaimTypes.Email);

                var value = claim?.Value;

                return value;
            }
        }
    }
}
