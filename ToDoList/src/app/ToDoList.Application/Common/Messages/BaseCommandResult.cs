﻿namespace ToDoList.Application.Common.Messages
{
    /// <summary>
    /// Ana Cevap
    /// </summary>
    public abstract class BaseCommandResult
    {
        /// <summary>
        /// Mesaj
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gizli Mesaj
        /// </summary>
        public string InternalMessage { get; set; }

        /// <summary>
        /// Ana Cevap
        /// </summary>
        public BaseCommandResult()
        {

        }

        /// <summary>
        /// Ana Cevap
        /// </summary>
        /// <param name="message"></param>
        /// <param name="internalMessage"></param>
        public BaseCommandResult(string message, string internalMessage)
        {
            Message = message;
            InternalMessage = internalMessage;
        }
    }
}
