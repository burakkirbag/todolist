﻿using MediatR;

namespace ToDoList.Application.TodoReminders.Queries.GetTodoReminders
{
    public class GetTodoRemindersQuery : IRequest<GetTodoRemindersQueryResult>
    {
    }
}
