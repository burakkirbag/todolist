﻿using System.Collections.Generic;

namespace ToDoList.Application.TodoReminders.Queries.GetTodoReminders
{
    public class GetTodoRemindersQueryResult
    {
        public List<TodoReminderDto> Reminders { get; set; }
    }
}
