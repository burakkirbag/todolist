﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Domain.Todos.Reminders;

namespace ToDoList.Application.TodoReminders.Queries.GetTodoReminders
{
    public class GetTodoRemindersQueryHandler : IRequestHandler<GetTodoRemindersQuery, GetTodoRemindersQueryResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoReminderRepository _todoReminderRepository;

        public GetTodoRemindersQueryHandler(IMediator mediator, ITodoReminderRepository todoReminderRepository)
        {
            _mediator = mediator;
            _todoReminderRepository = todoReminderRepository;
        }

        public async Task<GetTodoRemindersQueryResult> Handle(GetTodoRemindersQuery request, CancellationToken cancellationToken)
        {
            var result = new GetTodoRemindersQueryResult();

            var reminders = await _todoReminderRepository
                .Include(q => q.User)
                .Include(q => q.TodoItem)
                .Select(q => new TodoReminderDto
                {
                    Id = q.Id,
                    FirstName = q.User.FirstName,
                    LastName = q.User.LastName,
                    Email = q.User.Email,
                    Title = q.TodoItem.Title,
                    Note = q.TodoItem.Note
                })
                .ToListAsync();

            if (reminders.Count > 0)
            {
                result.Reminders = reminders;
            }

            return result;
        }
    }
}
