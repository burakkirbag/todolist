﻿using System;

namespace ToDoList.Application.TodoReminders.Queries.GetTodoReminders
{
    public class TodoReminderDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
    }
}
