﻿using MediatR;
using System;

namespace ToDoList.Application.TodoReminders.Commands.CreateTodoReminder
{
    public class CreateTodoReminderCommand : IRequest
    {
        public Guid TodoItemId { get; set; }
    }
}
