﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Domain.Todos.Items;
using ToDoList.Domain.Todos.Reminders;

namespace ToDoList.Application.TodoReminders.Commands.CreateTodoReminder

{    /// <summary>
     /// Hatırlatma Oluşturma
     /// </summary>
    public class CreateTodoReminderCommandHandler : AsyncRequestHandler<CreateTodoReminderCommand>
    {
        private readonly IMediator _mediator;
        private readonly ITodoReminderRepository _todoReminderRepository;
        private readonly ITodoItemRepository _todoItemRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Hatırlatma Oluşturma
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoReminderRepository"></param>
        /// <param name="todoItemRepository"></param>
        /// <param name="userContext"></param>
        public CreateTodoReminderCommandHandler(IMediator mediator, ITodoReminderRepository todoReminderRepository, ITodoItemRepository todoItemRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoReminderRepository = todoReminderRepository;
            _todoItemRepository = todoItemRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>        
        /// <returns></returns>
        protected override async Task Handle(CreateTodoReminderCommand request, CancellationToken cancellationToken)
        {
            var todoItem = await _todoItemRepository.FindByIdAndUserIdAsync(request.TodoItemId, _userContext.Id);
            if (todoItem != null)
            {
                await _todoReminderRepository.DeleteAsync(q => q.TodoItemId == request.TodoItemId && q.UserId == _userContext.Id, true, cancellationToken);

                var entity = new TodoReminder
                {
                    UserId = _userContext.Id,
                    TodoItemId = request.TodoItemId,
                    ReminderDate = todoItem.ReminderDate.Value,
                    IsSend = false
                };

                var todoReminder = await _todoReminderRepository.InsertAsync(entity, true, cancellationToken);
            }
        }
    }
}
