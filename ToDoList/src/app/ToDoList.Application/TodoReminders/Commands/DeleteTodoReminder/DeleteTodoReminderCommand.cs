﻿using MediatR;
using System;

namespace ToDoList.Application.TodoReminders.Commands.DeleteTodoReminder
{
    public class DeleteTodoReminderCommand : IRequest
    {
        public Guid? Id { get; set; }
        public Guid? TodoId { get; set; }
    }
}
