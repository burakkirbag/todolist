﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Domain.Todos.Reminders;

namespace ToDoList.Application.TodoReminders.Commands.DeleteTodoReminder
{    /// <summary>
     /// Hatırlatma Oluşturma
     /// </summary>
    public class CreateTodoReminderCommandHandler : AsyncRequestHandler<DeleteTodoReminderCommand>
    {
        private readonly IMediator _mediator;
        private readonly ITodoReminderRepository _todoReminderRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Hatırlatma Oluşturma
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoReminderRepository"></param>
        /// <param name="userContext"></param>
        public CreateTodoReminderCommandHandler(IMediator mediator, ITodoReminderRepository todoReminderRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoReminderRepository = todoReminderRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>        
        /// <returns></returns>
        protected override async Task Handle(DeleteTodoReminderCommand request, CancellationToken cancellationToken)
        {
            if (request.Id.HasValue)
            {
                var reminder = await _todoReminderRepository.FirstOrDefaultAsync(q => q.Id == request.Id.Value && q.UserId == _userContext.Id);
                if (reminder != null)
                {
                    await _todoReminderRepository.DeleteAsync(q => q.Id == request.Id.Value && q.UserId == _userContext.Id, true, cancellationToken);
                }
            }
            else if (request.TodoId.HasValue)
            {
                var reminder = await _todoReminderRepository.FirstOrDefaultAsync(q => q.TodoItemId == request.TodoId.Value && q.UserId == _userContext.Id);
                if (reminder != null)
                {
                    await _todoReminderRepository.DeleteAsync(q => q.TodoItemId == request.TodoId.Value && q.UserId == _userContext.Id, true, cancellationToken);
                }
            }
        }
    }
}
