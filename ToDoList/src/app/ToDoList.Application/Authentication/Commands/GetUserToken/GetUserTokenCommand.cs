﻿using MediatR;

namespace ToDoList.Application.Authentication.Commands.GetUserToken
{
    /// <summary>
    /// Kullanıcı Kimlik Doğrulama Isteği
    /// </summary>
    public class GetUserTokenCommand : IRequest<GetUserTokenCommandResult>
    {
        /// <summary>
        /// E-Posta Adresi
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Sifre
        /// </summary>
        public string Password { get; set; }
    }
}
