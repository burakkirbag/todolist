﻿namespace ToDoList.Application.Authentication.Commands.GetUserToken
{
    /// <summary>
    /// Kullanıcı Kimlik Doğrulama Cevabı
    /// </summary>
    public class GetUserTokenCommandResult
    {
        /// <summary>
        /// Adı
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Soyadı
        /// </summary>
        public string LastName { get; private set; }

        /// <summary>
        /// Kullanıcı Token
        /// </summary>
        public string Token { get; private set; }

        public GetUserTokenCommandResult(string firstName, string lastName, string token)
        {
            FirstName = firstName;
            LastName = lastName;
            Token = token;
        }
    }
}
