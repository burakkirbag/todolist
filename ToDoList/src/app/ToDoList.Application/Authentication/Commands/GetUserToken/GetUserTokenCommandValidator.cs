﻿using FluentValidation;
using ToDoList.Application.Common.Validation;

namespace ToDoList.Application.Authentication.Commands.GetUserToken
{
    /// <summary>
    /// Kullanıcı Token Olusturma Validasyon
    /// </summary>
    public class GetUserTokenCommandValidatorPipeLine : ValidationBehavior<GetUserTokenCommand, GetUserTokenCommandResult>
    {
        public GetUserTokenCommandValidatorPipeLine() : base(new GetUserTokenCommandValidator()) { }
    }

    /// <summary>
    /// Kullanıcı Token Olusturma Validasyon
    /// </summary>
    public class GetUserTokenCommandValidator : AbstractValidator<GetUserTokenCommand>
    {
        /// <summary>
        /// Kullanıcı Token Olusturma Validasyon
        /// </summary>
        public GetUserTokenCommandValidator()
        {
            RuleFor(r => r.Email)
                .NotNull()
                .NotEmpty().WithMessage("E-Posta girmelisiniz.")
                .MaximumLength(100).WithMessage("E-Posta Adresiniz en fazla 100 karakter olabilir.")
                .EmailAddress().WithMessage("Geçerli bir E-Posta Adresi girmelisiniz.");

            RuleFor(r => r.Password)
                .NotNull()
                .NotEmpty().WithMessage("Şifrenizi girmelisiniz.")
                .MinimumLength(4).WithMessage("Şifreniz en az 4 karakter olabilir.")
                .MaximumLength(8).WithMessage("Şifreniz en fazla 8 karakter olabilir.");
        }
    }
}
