﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Authentication.Commands.GenerateUserToken;
using ToDoList.Application.Authentication.GetUserToken.Exceptions;
using ToDoList.Core.Utilities.Extensions;
using ToDoList.Domain.Users;

namespace ToDoList.Application.Authentication.Commands.GetUserToken
{
    /// <summary>
    /// Kullanıcı Kimlik Doğrulama
    /// </summary>
    public class GetUserTokenCommandHandler : IRequestHandler<GetUserTokenCommand, GetUserTokenCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Kullanıcı Kimlik Doğrulama
        /// </summary>
        public GetUserTokenCommandHandler(IMediator mediator, IUserRepository userRepository)
        {
            _mediator = mediator;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetUserTokenCommandResult> Handle(GetUserTokenCommand request, CancellationToken cancellationToken)
        {
            string hashPassword = request.Password.ToMd5();

            var user = await _userRepository.FindByEmailAndPasswordAsync(request.Email, hashPassword);

            if (user == null)
                throw new LoginFailedException("Girmiş olduğunuz bilgiler ile eşleşen bir kullanıcı bulunamadı.");

            var generateUserTokenCommand = new GenerateUserTokenCommand
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email
            };

            var token = await _mediator.Send(generateUserTokenCommand);

            return new GetUserTokenCommandResult(user.FirstName, user.LastName, token.Token);
        }
    }
}
