﻿using MediatR;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Core.Authentication;

namespace ToDoList.Application.Authentication.Commands.GenerateUserToken
{
    /// <summary>
    /// Kullanıcı Token Oluşturma
    /// </summary>
    public class GenerateUserTokenCommandHandler : IRequestHandler<GenerateUserTokenCommand, GenerateUserTokenCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly ITokenBasedAuthenticator _authenticator;

        /// <summary>
        /// Kullanıcı Token Oluşturma
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="authenticator"></param>
        public GenerateUserTokenCommandHandler(IMediator mediator, ITokenBasedAuthenticator authenticator)
        {
            _mediator = mediator;
            _authenticator = authenticator;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GenerateUserTokenCommandResult> Handle(GenerateUserTokenCommand request, CancellationToken cancellationToken)
        {
            var claims = new Claim[] {
                new Claim(ClaimTypes.NameIdentifier,request.UserId.ToString()),
                new Claim(ClaimTypes.Name, $"{request.FirstName} {request.LastName}"),
                new Claim(ClaimTypes.Email, request.Email)
            };

            string token = _authenticator.Generate(claims);

            var result = new GenerateUserTokenCommandResult(token);

            return await Task.FromResult(result);
        }
    }
}
