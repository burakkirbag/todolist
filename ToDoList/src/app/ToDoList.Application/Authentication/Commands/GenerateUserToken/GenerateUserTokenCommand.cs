﻿using MediatR;
using System;

namespace ToDoList.Application.Authentication.Commands.GenerateUserToken
{
    /// <summary>
    /// Kullanıcı Token Oluşturma İsteği
    /// </summary>
    public class GenerateUserTokenCommand : IRequest<GenerateUserTokenCommandResult>
    {
        /// <summary>
        /// Kullanıcı Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Adı
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Soyadı
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// E-Posta Adresi
        /// </summary>
        public string Email { get; set; }
    }
}
