﻿namespace ToDoList.Application.Authentication.Commands.GenerateUserToken
{
    /// <summary>
    /// Kullanıcı Token Oluşturma Cevabı
    /// </summary>
    public class GenerateUserTokenCommandResult
    {
        /// <summary>
        /// Kullanıcı Token
        /// </summary>
        public string Token { get; private set; }

        /// <summary>
        /// Kullanıcı Token Oluşturma Cevabı
        /// </summary>
        public GenerateUserTokenCommandResult(string token)
        {
            Token = token;
        }
    }
}
