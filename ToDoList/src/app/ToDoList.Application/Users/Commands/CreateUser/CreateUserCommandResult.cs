﻿namespace ToDoList.Application.Users.Commands.CreateUser
{
    /// <summary>
    /// Kullanıcı Oluşturma Cevabı
    /// </summary>
    public class CreateUserCommandResult
    {
        /// <summary>
        /// Adı
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Soyadı
        /// </summary>
        public string LastName { get; private set; }

        /// <summary>
        /// Kullanıcı Token
        /// </summary>
        public string Token { get; private set; }

        public CreateUserCommandResult(string firstName, string lastName, string token)
        {
            FirstName = firstName;
            LastName = lastName;
            Token = token;
        }
    }
}
