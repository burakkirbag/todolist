﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Authentication.Commands.GenerateUserToken;
using ToDoList.Application.Users.Commands.CreateUser.Exceptions;
using ToDoList.Core.UnitOfWork;
using ToDoList.Core.Utilities.Extensions;
using ToDoList.Domain.Users;

namespace ToDoList.Application.Users.Commands.CreateUser
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, CreateUserCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;

        public CreateUserCommandHandler(IMediator mediator, IUnitOfWork unitOfWork, IUserRepository userRepository)
        {
            _mediator = mediator;
            _unitOfWork = unitOfWork;
            _userRepository = userRepository;
        }

        public async Task<CreateUserCommandResult> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            bool isExistsEmail = await _userRepository.IsExistsEmailAsync(request.Email);
            if (isExistsEmail)
                throw new EmailAdressExistsException("Girmiş olduğunuz e-posta adresi kullanılmaktadır.");

            var entity = new User
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Password = request.Password.ToMd5(),
                RegistrationTime = DateTimeOffset.Now
            };

            await _unitOfWork.BeginAsync(cancellationToken);

            var createdUser = await _userRepository.InsertAsync(entity, false, cancellationToken);

            if (createdUser == null)
                throw new CreateUserFailedException("Kullanıcı oluşturma işlemi sırasında bir hata ile karşılaşıldı. İşleminiz tamamlanamadı.");

            var generateUserTokenCommand = new GenerateUserTokenCommand
            {
                UserId = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Email = entity.Email
            };

            var token = await _mediator.Send(generateUserTokenCommand);

            await _unitOfWork.SaveChangesAsync();
            await _unitOfWork.CommitAsync();

            return new CreateUserCommandResult(request.FirstName, request.LastName, token.Token);
        }
    }
}
