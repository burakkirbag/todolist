﻿using MediatR;

namespace ToDoList.Application.Users.Commands.CreateUser
{
    /// <summary>
    /// Kullanıcı Oluşturma İsteği
    /// </summary>
    public class CreateUserCommand : IRequest<CreateUserCommandResult>
    {
        /// <summary>
        /// Adı
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Soyadı
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// E-Posta Adresi
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Şifre
        /// </summary>
        public string Password { get; set; }
    }
}
