﻿using FluentValidation;
using ToDoList.Application.Common.Validation;

namespace ToDoList.Application.Users.Commands.CreateUser
{
    public class CreateUserCommandValidatorPipeLine : ValidationBehavior<CreateUserCommand, CreateUserCommandResult>
    {
        public CreateUserCommandValidatorPipeLine() : base(new CreateUserCommandValidator()) { }
    }

    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(r => r.FirstName)
                .NotNull()
                .NotEmpty().WithMessage("Adınızı girmelisiniz.")
                .MaximumLength(50).WithMessage("Adınız en fazla 50 karakter olabilir.");

            RuleFor(r => r.LastName)
                .NotNull()
                .NotEmpty().WithMessage("Soyadınızı girmelisiniz.")
                .MaximumLength(50).WithMessage("Soyadınız en fazla 50 karakter olabilir.");

            RuleFor(r => r.Email)
                .NotNull()
                .NotEmpty().WithMessage("E-Posta girmelisiniz.")
                .MaximumLength(100).WithMessage("E-Posta Adresiniz en fazla 100 karakter olabilir.")
                .EmailAddress().WithMessage("Geçerli bir E-Posta Adresi girmelisiniz.");

            RuleFor(r => r.Password)
                .NotNull()
                .NotEmpty().WithMessage("Şifrenizi girmelisiniz.")
                .MinimumLength(4).WithMessage("Şifreniz en az 4 karakter olabilir.")
                .MaximumLength(8).WithMessage("Şifreniz en fazla 8 karakter olabilir.");
        }
    }
}
