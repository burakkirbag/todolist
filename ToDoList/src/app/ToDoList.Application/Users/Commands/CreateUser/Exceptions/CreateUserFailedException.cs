﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.Users.Commands.CreateUser.Exceptions
{
    public class CreateUserFailedException : Exception
    {
        public CreateUserFailedException()
        {
        }

        public CreateUserFailedException(string message) : base(message)
        {
        }

        public CreateUserFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CreateUserFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
