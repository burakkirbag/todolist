﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.Users.Commands.CreateUser.Exceptions
{
    public class EmailAdressExistsException : Exception
    {
        public EmailAdressExistsException()
        {
        }

        public EmailAdressExistsException(string message) : base(message)
        {
        }

        public EmailAdressExistsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmailAdressExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
