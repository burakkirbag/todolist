﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoItems.Commands.UpdateTodoItem.Exceptions;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoLists.Commands.UpdateTodoList
{
    /// <summary>
    /// Liste Düzenleme
    /// </summary>
    public class UpdateTodoListCommandHandler : IRequestHandler<UpdateTodoListCommand, UpdateTodoListCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoListRepository _todoListRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Liste Düzenleme
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoListRepository"></param>
        /// <param name="userContext"></param>
        public UpdateTodoListCommandHandler(IMediator mediator, ITodoListRepository todoListRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoListRepository = todoListRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpdateTodoListCommandResult> Handle(UpdateTodoListCommand request, CancellationToken cancellationToken)
        {
            var todoList = await _todoListRepository.FindByIdAndUserIdAsync(request.Id, _userContext.Id);

            if (todoList == null)
                throw new UpdateTodoItemNotFoundException("Düzenlemek istediğiniz kayıt bulunamadı.");

            todoList.Title = request.Title;
            todoList.Description = request.Description;
            todoList.Status = request.Status;

            var updateEntity = await _todoListRepository.UpdateAsync(todoList, true, cancellationToken);

            return new UpdateTodoListCommandResult { Message = "Görev listesi güncellenmiştir." };
        }
    }
}
