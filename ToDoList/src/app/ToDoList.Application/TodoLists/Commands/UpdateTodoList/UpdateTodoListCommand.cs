﻿using MediatR;
using System;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoLists.Commands.UpdateTodoList
{
    /// <summary>
    /// Liste Düzenleme İsteği
    /// </summary>
    public class UpdateTodoListCommand : IRequest<UpdateTodoListCommandResult>
    {
        /// <summary>
        /// Liste Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Başlık
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Durumu
        /// </summary>
        public TodoStatus Status { get; set; }
    }
}
