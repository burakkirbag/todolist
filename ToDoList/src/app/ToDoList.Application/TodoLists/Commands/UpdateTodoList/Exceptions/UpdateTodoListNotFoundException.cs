﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.Todos.TodoLists.UpdateTodoItem.Exceptions
{
    public class UpdateTodoListNotFoundException : Exception
    {
        public UpdateTodoListNotFoundException()
        {
        }

        public UpdateTodoListNotFoundException(string message) : base(message)
        {
        }

        public UpdateTodoListNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UpdateTodoListNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
