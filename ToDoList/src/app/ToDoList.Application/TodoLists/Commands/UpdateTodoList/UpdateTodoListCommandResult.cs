﻿using ToDoList.Application.Common.Messages;

namespace ToDoList.Application.TodoLists.Commands.UpdateTodoList
{
    /// <summary>
    /// Liste Düzenleme Cevabı
    /// </summary>
    public class UpdateTodoListCommandResult : BaseCommandResult
    {
    }
}
