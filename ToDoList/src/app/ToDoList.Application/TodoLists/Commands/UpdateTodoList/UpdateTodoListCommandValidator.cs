﻿using FluentValidation;
using System;
using ToDoList.Application.Common.Validation;

namespace ToDoList.Application.TodoLists.Commands.UpdateTodoList
{
    /// <summary>
    /// Liste Düzenleme Validasyon
    /// </summary>
    public class UpdateTodoListCommandValidatorPipeLine : ValidationBehavior<UpdateTodoListCommand, UpdateTodoListCommandResult>
    {
        public UpdateTodoListCommandValidatorPipeLine() : base(new UpdateTodoListCommandValidator()) { }
    }

    /// <summary>
    /// Liste Düzenleme Validasyonu
    /// </summary>
    public class UpdateTodoListCommandValidator : AbstractValidator<UpdateTodoListCommand>
    {
        /// <summary>
        /// Liste Düzenleme Validasyonu
        /// </summary>
        public UpdateTodoListCommandValidator()
        {
            RuleFor(r => r.Id)
                .NotNull()
                .NotEmpty()
                .NotEqual(Guid.Empty).WithMessage("Düzenlenecek kayıt bilgisi boş olamaz.");

            RuleFor(r => r.Title)
               .NotNull()
               .NotEmpty().WithMessage("Başlık girmelisiniz.")
               .MaximumLength(100).WithMessage("Başlık en fazla 100 karakter olabilir.");

            RuleFor(r => r.Description)
                .MaximumLength(500).WithMessage("Açıklama en fazla 500 karakter olabilir.");
        }
    }
}
