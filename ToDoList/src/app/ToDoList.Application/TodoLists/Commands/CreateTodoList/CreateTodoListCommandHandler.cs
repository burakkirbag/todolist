﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoLists.Commands.CreateTodoList
{
    /// <summary>
    /// Liste Oluşturma
    /// </summary>
    public class CreateTodoListCommandHandler : IRequestHandler<CreateTodoListCommand, CreateTodoListCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoListRepository _todoListRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Liste Oluşturma
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoListRepository"></param>
        /// <param name="userContext"></param>
        public CreateTodoListCommandHandler(IMediator mediator, ITodoListRepository todoListRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoListRepository = todoListRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateTodoListCommandResult> Handle(CreateTodoListCommand request, CancellationToken cancellationToken)
        {
            var entity = new TodoList
            {
                UserId = _userContext.Id,
                Title = request.Title,
                Description = request.Description,
                Status = TodoStatus.Active,
                CreationTime = DateTimeOffset.Now
            };

            var todoList = await _todoListRepository.InsertAsync(entity, true, cancellationToken);

            return new CreateTodoListCommandResult { Message = "Yeni görev listesi eklenmiştir." };
        }
    }
}
