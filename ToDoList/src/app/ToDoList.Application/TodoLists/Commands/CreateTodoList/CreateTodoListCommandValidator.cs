﻿using FluentValidation;
using ToDoList.Application.Common.Validation;

namespace ToDoList.Application.TodoLists.Commands.CreateTodoList
{
    /// <summary>
    /// Görev Liste Olusturma Validasyon
    /// </summary>
    public class CreateTodoListCommandValidatorPipeLine : ValidationBehavior<CreateTodoListCommand, CreateTodoListCommandResult>
    {
        public CreateTodoListCommandValidatorPipeLine() : base(new CreateTodoListCommandValidator()) { }
    }

    /// <summary>
    /// Görev Liste Oluşturma Validasyonu
    /// </summary>
    public class CreateTodoListCommandValidator : AbstractValidator<CreateTodoListCommand>
    {
        /// <summary>
        /// Görev Liste Oluşturma Validasyonu
        /// </summary>
        public CreateTodoListCommandValidator()
        {
            RuleFor(r => r.Title)
               .NotNull()
               .NotEmpty().WithMessage("Başlık girmelisiniz.")
               .MaximumLength(100).WithMessage("Başlık en fazla 100 karakter olabilir.");

            RuleFor(r => r.Description)
                .MaximumLength(500).WithMessage("Açıklama en fazla 500 karakter olabilir.");
        }
    }
}
