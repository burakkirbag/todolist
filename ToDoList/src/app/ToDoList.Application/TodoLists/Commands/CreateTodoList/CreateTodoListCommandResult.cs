﻿using ToDoList.Application.Common.Messages;

namespace ToDoList.Application.TodoLists.Commands.CreateTodoList
{
    /// <summary>
    /// Liste Oluşturma Cevabı
    /// </summary>
    public class CreateTodoListCommandResult : BaseCommandResult
    {
    }
}
