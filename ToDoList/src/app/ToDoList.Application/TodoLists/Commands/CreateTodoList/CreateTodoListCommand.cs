﻿using MediatR;

namespace ToDoList.Application.TodoLists.Commands.CreateTodoList
{
    /// <summary>
    /// Liste Oluşturma İsteği
    /// </summary>
    public class CreateTodoListCommand : IRequest<CreateTodoListCommandResult>
    {
        /// <summary>
        /// Başlık
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama
        /// </summary>
        public string Description { get; set; }
    }
}
