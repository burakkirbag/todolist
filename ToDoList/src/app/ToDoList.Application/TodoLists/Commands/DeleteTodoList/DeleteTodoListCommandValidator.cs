﻿using FluentValidation;
using System;
using ToDoList.Application.Common.Validation;

namespace ToDoList.Application.TodoLists.Commands.DeleteTodoList
{
    /// <summary>
    /// Liste Silme Validasyon
    /// </summary>
    public class DeleteTodoListCommandValidatorPipeLine : ValidationBehavior<DeleteTodoListCommand, DeleteTodoListCommandResult>
    {
        public DeleteTodoListCommandValidatorPipeLine() : base(new DeleteTodoListCommandValidator()) { }
    }

    /// <summary>
    /// Liste Silme Validasyonu
    /// </summary>
    public class DeleteTodoListCommandValidator : AbstractValidator<DeleteTodoListCommand>
    {
        /// <summary>
        /// Liste Silme Validasyonu
        /// </summary>
        public DeleteTodoListCommandValidator()
        {
            RuleFor(r => r.Id)
                .NotNull()
                .NotEmpty()
                .NotEqual(Guid.Empty).WithMessage("Silinecek kayıt bilgisi boş olamaz.");
        }
    }
}
