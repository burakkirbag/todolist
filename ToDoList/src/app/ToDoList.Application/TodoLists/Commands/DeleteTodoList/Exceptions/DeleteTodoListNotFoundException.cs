﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.TodoLists.Commands.DeleteTodoList.Exceptions
{
    public class DeleteTodoListNotFoundException : Exception
    {
        public DeleteTodoListNotFoundException()
        {
        }

        public DeleteTodoListNotFoundException(string message) : base(message)
        {
        }

        public DeleteTodoListNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DeleteTodoListNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
