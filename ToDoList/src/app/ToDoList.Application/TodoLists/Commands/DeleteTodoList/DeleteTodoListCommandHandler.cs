﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoLists.Commands.DeleteTodoList.Exceptions;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoLists.Commands.DeleteTodoList
{
    /// <summary>
    /// Liste Silme
    /// </summary>
    public class DeleteTodoListCommandHandler : IRequestHandler<DeleteTodoListCommand, DeleteTodoListCommandResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoListRepository _todoListRepository;
        private readonly IUserContext _userContext;

        /// <summary>
        /// Liste Silme
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="todoListRepository"></param>
        /// <param name="userContext"></param>
        public DeleteTodoListCommandHandler(IMediator mediator, ITodoListRepository todoListRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoListRepository = todoListRepository;
            _userContext = userContext;
        }

        /// <summary>
        /// Handle
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DeleteTodoListCommandResult> Handle(DeleteTodoListCommand request, CancellationToken cancellationToken)
        {
            var todoList = await _todoListRepository.FindByIdAndUserIdAsync(request.Id, _userContext.Id);

            if (todoList == null)
                throw new DeleteTodoListNotFoundException("Silmek istediğiniz kayıt bulunamadı.");

            await _todoListRepository.DeleteAsync(request.Id, true, cancellationToken);

            return new DeleteTodoListCommandResult { Message = "Görev listesi silinmiştir." };
        }
    }
}
