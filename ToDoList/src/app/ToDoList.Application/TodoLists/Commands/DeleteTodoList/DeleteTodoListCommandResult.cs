﻿using ToDoList.Application.Common.Messages;

namespace ToDoList.Application.TodoLists.Commands.DeleteTodoList
{
    /// <summary>
    /// Liste Silme Cevabı
    /// </summary>
    public class DeleteTodoListCommandResult : BaseCommandResult
    {
    }
}
