﻿using MediatR;
using System;

namespace ToDoList.Application.TodoLists.Commands.DeleteTodoList
{
    /// <summary>
    /// Liste Silme İsteği
    /// </summary>
    public class DeleteTodoListCommand : IRequest<DeleteTodoListCommandResult>
    {
        /// <summary>
        /// Liste Id
        /// </summary>
        public Guid Id { get; set; }
    }
}
