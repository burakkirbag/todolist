﻿using System;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoLists.Queries.GetTodoList
{
    /// <summary>
    /// Görev Listesi
    /// </summary>
    public class TodoListDto
    {
        /// <summary>
        /// Liste Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Başlık
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Açıklama
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Durumu
        /// </summary>
        public TodoStatus Status { get; set; }

        /// <summary>
        /// Durumu
        /// </summary>
        public string StatusText { get; set; }

        /// <summary>
        /// Oluşturulma Zamanı
        /// </summary>
        public DateTimeOffset CreationTime { get; set; }
    }
}
