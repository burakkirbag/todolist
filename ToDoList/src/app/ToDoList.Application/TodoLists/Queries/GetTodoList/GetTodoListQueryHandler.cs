﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoLists.Queries.GetTodoList.Exceptions;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoLists.Queries.GetTodoList
{
    public class GetTodoListQueryHandler : IRequestHandler<GetTodoListQuery, GetTodoListQueryResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoListRepository _todoListRepository;
        private readonly IUserContext _userContext;

        public GetTodoListQueryHandler(IMediator mediator, ITodoListRepository todoListRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoListRepository = todoListRepository;
            _userContext = userContext;
        }

        public async Task<GetTodoListQueryResult> Handle(GetTodoListQuery request, CancellationToken cancellationToken)
        {
            var result = new GetTodoListQueryResult();

            var todoList = await _todoListRepository
                .Where(q => q.UserId.Equals(_userContext.Id))
                .Select(q => new TodoListDto
                {
                    Id = q.Id,
                    Title = q.Title,
                    Description = q.Description,
                    Status = q.Status,
                    StatusText = (q.Status == TodoStatus.Completed ? "Tamamlandı" : "Aktif"),
                    CreationTime = q.CreationTime
                })
                .FirstOrDefaultAsync();

            result.TodoList = todoList ?? throw new GetTodoListNotFoundException("Kayıt bulunamadı.");

            return result;
        }
    }
}
