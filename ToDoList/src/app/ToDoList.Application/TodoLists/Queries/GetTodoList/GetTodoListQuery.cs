﻿using MediatR;
using System;

namespace ToDoList.Application.TodoLists.Queries.GetTodoList
{
    public class GetTodoListQuery : IRequest<GetTodoListQueryResult>
    {
        public Guid Id { get; set; }
    }
}
