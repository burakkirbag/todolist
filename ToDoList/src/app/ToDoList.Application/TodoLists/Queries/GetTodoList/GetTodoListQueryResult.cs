﻿namespace ToDoList.Application.TodoLists.Queries.GetTodoList
{
    public class GetTodoListQueryResult
    {
        public TodoListDto TodoList { get; set; }
    }
}
