﻿using System;
using System.Runtime.Serialization;

namespace ToDoList.Application.TodoLists.Queries.GetTodoList.Exceptions
{
    public class GetTodoListNotFoundException : Exception
    {
        public GetTodoListNotFoundException()
        {
        }

        public GetTodoListNotFoundException(string message) : base(message)
        {
        }

        public GetTodoListNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GetTodoListNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
