﻿using System.Collections.Generic;
using ToDoList.Application.TodoLists.Queries.GetTodoList;

namespace ToDoList.Application.TodoLists.Queries.GetTodoLists
{
    public class GetTodoListsQueryResult
    {
        public List<TodoListDto> TodoLists { get; set; }

        public GetTodoListsQueryResult()
        {
            TodoLists = new List<TodoListDto>();
        }
    }
}
