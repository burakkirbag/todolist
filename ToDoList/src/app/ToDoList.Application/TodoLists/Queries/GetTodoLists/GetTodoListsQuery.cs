﻿using MediatR;

namespace ToDoList.Application.TodoLists.Queries.GetTodoLists
{
    public class GetTodoListsQuery : IRequest<GetTodoListsQueryResult>
    {
    }
}
