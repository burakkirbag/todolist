﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Application.Common.Context;
using ToDoList.Application.TodoLists.Queries.GetTodoList;
using ToDoList.Domain.Todos;

namespace ToDoList.Application.TodoLists.Queries.GetTodoLists
{
    public class GetTodoListsQueryHandler : IRequestHandler<GetTodoListsQuery, GetTodoListsQueryResult>
    {
        private readonly IMediator _mediator;
        private readonly ITodoListRepository _todoListRepository;
        private readonly IUserContext _userContext;

        public GetTodoListsQueryHandler(IMediator mediator, ITodoListRepository todoListRepository, IUserContext userContext)
        {
            _mediator = mediator;
            _todoListRepository = todoListRepository;
            _userContext = userContext;
        }

        public async Task<GetTodoListsQueryResult> Handle(GetTodoListsQuery request, CancellationToken cancellationToken)
        {
            var result = new GetTodoListsQueryResult();

            var todoLists = await _todoListRepository
                .Where(q => q.UserId.Equals(_userContext.Id))
                .Select(q => new TodoListDto
                {
                    Id = q.Id,
                    Title = q.Title,
                    Description = q.Description,
                    Status = q.Status,
                    StatusText = (q.Status == TodoStatus.Completed ? "Tamamlandı" : "Aktif"),
                    CreationTime = q.CreationTime
                })
                .ToListAsync();

            if (todoLists.Count > 0)
            {
                result.TodoLists = todoLists;
            }

            return result;
        }
    }
}
