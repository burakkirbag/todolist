﻿using MediatR;
using ToDoList.Core.BackgroundJobs;
using ToDoList.Core.Notification.Email.Smtp;
using ToDoList.Domain.Todos.Reminders;

namespace ToDoList.Infrastructure.BackgroundJobs.HangFire.Jobs
{
    public class EmailNotificationJob : IJob
    {
        private readonly ITodoReminderRepository _todoReminderRepository;

        private readonly ISmtpEmailSender _smtpEmailSender;

        public EmailNotificationJob(ITodoReminderRepository todoReminderRepository, ISmtpEmailSenderConfiguration smtpEmailSenderConfiguration, ISmtpEmailSender smtpEmailSender)
        {
            SetEmailConfig(smtpEmailSenderConfiguration);

            _todoReminderRepository = todoReminderRepository;
            _smtpEmailSender = smtpEmailSender;
        }

        public void Execute()
        {
            var reminders = _todoReminderRepository.GetRemindersWithUserAndTodo();
            if (reminders != null && reminders.Count > 0)
            {
                foreach (var reminder in reminders)
                {
                    _smtpEmailSender.Send($"{reminder.Email}", "ToDoList Hatırlatma", $"{reminder.Title} başlıklı göreviniz için hatırlatmadır.");
                    _todoReminderRepository.Delete(q => q.Id == reminder.Id, true);
                }
            }
        }

        // TODO : Move to AppSettings
        private void SetEmailConfig(ISmtpEmailSenderConfiguration smtpEmailSenderConfiguration)
        {
            smtpEmailSenderConfiguration.DefaultFromAddress = "todolisttest@yandex.com";
            smtpEmailSenderConfiguration.DefaultFromDisplayName = "ToDoList";
            smtpEmailSenderConfiguration.Password = "123456*";
            smtpEmailSenderConfiguration.UserName = "todolisttest@yandex.com";
            smtpEmailSenderConfiguration.Host = "smtp.yandex.ru";
            smtpEmailSenderConfiguration.EnableSsl = true;
            smtpEmailSenderConfiguration.Port = 587;
            smtpEmailSenderConfiguration.UseDefaultCredentials = false;
        }
    }
}
