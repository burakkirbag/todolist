﻿using Hangfire;
using MediatR;
using System;
using ToDoList.Core.BackgroundJobs;
using ToDoList.Domain.Todos.Reminders;
using ToDoList.Infrastructure.BackgroundJobs.HangFire.Jobs;

namespace ToDoList.Infrastructure.BackgroundJobs.HangFire
{
    public class HangfireJobBuilder : IJobBuilder
    {
        private readonly IMediator _mediator;

        private readonly IJob _job;

        private readonly ITodoReminderRepository _todoReminderRepository;

        public HangfireJobBuilder(IMediator mediator, IJob job)
        {
            _mediator = mediator;
            _job = job;
        }

        public void Build()
        {
            RecurringJob.AddOrUpdate("EmailNotificationJob", () => _job.Execute(), Cron.Minutely,
                TimeZoneInfo.Local);
        }
    }
}
