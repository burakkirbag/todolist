﻿using Microsoft.Extensions.Logging;

namespace ToDoList.Infrastructure.Logger
{
    public class FileLogProvider : ILoggerProvider
    {
        public ILogger CreateLogger(string category)
        {
            return new FileLogger();
        }
        public void Dispose()
        {

        }
    }
}
