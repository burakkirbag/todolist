﻿namespace ToDoList.Infrastructure.Authentication.Jwt
{
    public class JwtAuthSettings
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string SecurityKey { get; set; }

        /// <summary>
        /// In minutes
        /// </summary>
        public double TokenExpiration { get; set; }

        /// <summary>
        /// In minutes
        /// </summary>
        public double RefreshTokenExpiration { get; set; }
    }
}
