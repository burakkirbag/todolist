﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ToDoList.Core.Authentication;

namespace ToDoList.Infrastructure.Authentication.Jwt
{
    public class JwtAuthenticator : ITokenBasedAuthenticator, IAuthenticator
    {
        private readonly IOptions<JwtAuthSettings> _authSettings;

        public JwtAuthenticator(IOptions<JwtAuthSettings> authSettings)
        {
            _authSettings = authSettings;
        }

        public string Generate(Claim[] claims)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            SecurityKey securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authSettings.Value.SecurityKey));
            var token = new JwtSecurityToken(
                issuer: _authSettings.Value.Issuer,
                audience: _authSettings.Value.Audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(_authSettings.Value.TokenExpiration),
                signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)
            );

            return tokenHandler.WriteToken(token);
        }

        public bool Validate(string token)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            TokenValidationParameters validationParameters = GetTokenValidationParameters();

            SecurityToken validatedToken;
            ClaimsPrincipal principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
            return true;
        }

        private TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateAudience = true,
                ValidAudience = _authSettings.Value.Audience,
                ValidateIssuer = true,
                ValidIssuer = _authSettings.Value.Issuer,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authSettings.Value.SecurityKey)),
                ClockSkew = TimeSpan.Zero
            };
        }
    }
}
