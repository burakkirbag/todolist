﻿using System;
using System.Collections.Generic;
using ToDoList.Core.Data;
using ToDoList.Core.Domain.Entities;
using ToDoList.Domain.Todos;
using ToDoList.Domain.Todos.Items;
using ToDoList.Domain.Todos.Reminders;

namespace ToDoList.Domain.Users
{
    public class User : Entity<Guid>, ISoftDelete
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public DateTimeOffset RegistrationTime { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<TodoList> TodoLists { get; set; }

        public ICollection<TodoItem> TodoItems { get; set; }

        public ICollection<TodoReminder> TodoReminders { get; set; }

        public override object[] GetKeys()
        {
            return new object[] { Id };
        }
    }
}
