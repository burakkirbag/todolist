﻿using System;
using System.Threading.Tasks;
using ToDoList.Core.Domain.Repositories;

namespace ToDoList.Domain.Users
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        Task<bool> IsExistsEmailAsync(string email);

        Task<User> FindByEmailAndPasswordAsync(string email, string password);
    }
}
