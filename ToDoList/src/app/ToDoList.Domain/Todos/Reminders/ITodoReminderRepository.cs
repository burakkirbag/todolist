﻿using System;
using System.Collections.Generic;
using ToDoList.Core.Domain.Repositories;
using ToDoList.Domain.Todos.Reminders.ComplexTypes;

namespace ToDoList.Domain.Todos.Reminders
{
    public interface ITodoReminderRepository : IRepository<TodoReminder, Guid>
    {
        List<TodoReminderWithUserAndTodo> GetRemindersWithUserAndTodo();
    }
}
