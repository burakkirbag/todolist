﻿using System;

namespace ToDoList.Domain.Todos.Reminders.ComplexTypes
{
    public class TodoReminderWithUserAndTodo
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
    }
}
