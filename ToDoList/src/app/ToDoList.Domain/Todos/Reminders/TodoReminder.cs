﻿using System;
using ToDoList.Core.Domain.Entities;
using ToDoList.Domain.Todos.Items;
using ToDoList.Domain.Users;

namespace ToDoList.Domain.Todos.Reminders
{
    public class TodoReminder : Entity<Guid>
    {
        public Guid UserId { get; set; }
        public Guid TodoItemId { get; set; }
        public DateTimeOffset ReminderDate { get; set; }
        public bool IsSend { get; set; }
        public virtual User User { get; set; }
        public virtual TodoItem TodoItem { get; set; }

        public override object[] GetKeys()
        {
            return new object[] { Id, UserId, TodoItemId };
        }
    }
}
