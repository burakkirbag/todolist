﻿using System;
using System.Collections.Generic;
using ToDoList.Core.Data;
using ToDoList.Core.Domain.Entities;
using ToDoList.Domain.Todos.Reminders;
using ToDoList.Domain.Users;

namespace ToDoList.Domain.Todos.Items
{
    public class TodoItem : Entity<Guid>, ISoftDelete
    {
        public Guid UserId { get; set; }

        public Guid ListId { get; set; }

        public string Title { get; set; }

        public string Note { get; set; }

        public DateTimeOffset? DueDate { get; set; }

        public DateTimeOffset? ReminderDate { get; set; }

        public TodoStatus Status { get; set; }

        public DateTimeOffset CreationTime { get; set; }

        public bool IsDeleted { get; set; }

        public virtual User User { get; set; }

        public virtual TodoList List { get; set; }

        public ICollection<TodoReminder> Reminders { get; set; }

        public override object[] GetKeys()
        {
            return new object[] { Id, UserId, ListId };
        }
    }
}
