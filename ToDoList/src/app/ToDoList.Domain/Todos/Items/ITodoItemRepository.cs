﻿using System;
using System.Threading.Tasks;
using ToDoList.Core.Domain.Repositories;

namespace ToDoList.Domain.Todos.Items
{
    public interface ITodoItemRepository : IRepository<TodoItem, Guid>
    {
        Task<int> GetCountByListIdAndUserId(Guid listId, Guid userId);

        Task<int> GetActiveStatusCountByListIdAndUserId(Guid listId, Guid userId);

        Task<TodoItem> FindByIdAndUserIdAsync(Guid id, Guid userId);
    }
}
