﻿using System;
using System.Collections.Generic;
using ToDoList.Core.Data;
using ToDoList.Core.Domain.Entities;
using ToDoList.Domain.Todos.Items;
using ToDoList.Domain.Users;

namespace ToDoList.Domain.Todos
{
    public class TodoList : Entity<Guid>, ISoftDelete
    {
        public Guid UserId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public TodoStatus Status { get; set; }

        public DateTimeOffset CreationTime { get; set; }

        public bool IsDeleted { get; set; }

        public virtual User User { get; set; }

        public ICollection<TodoItem> Items { get; set; }

        public override object[] GetKeys()
        {
            return new object[] { Id, UserId };
        }
    }
}
