﻿namespace ToDoList.Domain.Todos
{
    public enum TodoStatus : int
    {
        /// <summary>
        /// Aktif
        /// </summary>
        Active = 1,

        /// <summary>
        /// Tamamlandi
        /// </summary>
        Completed = 2
    }
}
