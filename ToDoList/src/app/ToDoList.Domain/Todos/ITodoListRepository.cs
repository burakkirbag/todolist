﻿using System;
using System.Threading.Tasks;
using ToDoList.Core.Domain.Repositories;

namespace ToDoList.Domain.Todos
{
    public interface ITodoListRepository : IRepository<TodoList, Guid>
    {
        Task<TodoList> FindByIdAndUserIdAsync(Guid id, Guid userId);
    }
}
