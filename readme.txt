"ToDoList.WebAPI" projesi içerisinde bulunan appsettings.json dosyasından "ConnectionString" bilgisi değiştirilmelidir.


"ToDoList.WebUI" projesi içerisinde bulunan "api-manager.js" dosyasındaki "axios.defaults.baseURL = 'http://localhost:53240';" satırı Web Api adresine göre değiştirilmelidir.

Arka plan görevleri için HangFire kullanılmıştır. WebApiProjeAdresi/hangfire adresi uzerinden tanımlı islere erisilebilir.

"Application" katmanı içerisinde CQRS yaklaşımını uygulamak için MediatR kütüphanesi kullanılmıştır.

Web Api tarafında kimlik doğrulama işlemleri için "JWT(Json Web Token)" kullanılmıştır.

Web UI tarafinda api iletisimi icin "axios" kutuphanesi kullanilmistir.

Web Apiyi tuketecek kisilere kolaylık saglamak icin Swagger implementasyonu yapilmistir."WebApiProjeAdresi/swagger" uzerinden erisilebilir.